package com.dev.logistic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.dev.logistic.entity.BillofLanding;
import com.dev.logistic.repository.BillofLandingRepository;
import com.dev.logistic.service.BillofLandingService;
import com.dev.logistic.service.util.PagingAndSortingRequest;

@Service
public class BillofLandingServiceImpl implements BillofLandingService {

	@Autowired
	private BillofLandingRepository billofLandingRepository;
	
	@Autowired
	private PagingAndSortingRequest pagingRequest;
	
	public BillofLanding create(BillofLanding bol) {
		return billofLandingRepository.save(bol);
	}

	public BillofLanding find(String id) {
		return billofLandingRepository.findOne(id);
	}

	public BillofLanding update(BillofLanding bol, BillofLanding _bol) {
		
		bol.setBol(_bol.getBol());
		bol.setThirdparty(_bol.getThirdparty());
		bol.setDriver(_bol.getDriver());
		bol.setOrigin(_bol.getOrigin());
		bol.setDestination(_bol.getDestination());
		bol.setEmgergencynum(_bol.getEmgergencynum());
		bol.setCod(_bol.getCod());
		bol.setValue(_bol.getValue());
		bol.setNotes(_bol.getNotes());
		bol.setItemPieces(_bol.getItemPieces());
		bol.setItemDesc(_bol.getItemDesc());
		bol.setItemLbs(_bol.getItemLbs());
		bol.setItemType(_bol.getItemType());
		bol.setItemNMFC(_bol.getItemNMFC());
		bol.setItemClass(_bol.getItemClass());
		bol.setItemHM(_bol.getItemHM());
		
		return billofLandingRepository.save(bol);
	}

	public void delete(BillofLanding bol) {
		billofLandingRepository.delete(bol);
	}

	public List<BillofLanding> list() {
		return billofLandingRepository.findAll();
	}

	@Override
	public Page<BillofLanding> list(int start, int end, String sortBy, String sortType) {
		PageRequest pageRequest = pagingRequest.getPageRequest(start, end, sortBy, sortType);
		return billofLandingRepository.findAll(pageRequest);
	}

}
