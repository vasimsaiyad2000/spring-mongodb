package com.dev.logistic.service;

import com.dev.logistic.entity.LoadInfoAdditionalNumbers;

/**
 * This interface define service layer operations for loadinfoaddtionalnumbers.This inherit
 * BaseService interface to define common CRUD operations. 
 * 
 * LoadInfoAdditionalNumbersServiceImpl class implements operations define in this interface.
 * 
 * @author vasim
 * 
 */
public interface LoadInfoAdditionalNumbersService extends BaseService<LoadInfoAdditionalNumbers> {

}
