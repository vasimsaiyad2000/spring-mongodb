package com.dev.logistic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.dev.logistic.entity.AddressEnvelop;
import com.dev.logistic.repository.AddressEnvelopRepository;
import com.dev.logistic.service.AddressEnvelopService;
import com.dev.logistic.service.util.PagingAndSortingRequest;

@Service
public class AddressEnvelopServiceImpl implements AddressEnvelopService {

	@Autowired
	private AddressEnvelopRepository addressRepository;
	
	@Autowired
	private PagingAndSortingRequest pagingRequest;
	
	public AddressEnvelop create(AddressEnvelop address) {
		return addressRepository.save(address);
	}

	public AddressEnvelop find(String id) {
		return addressRepository.findOne(id);
	}

	public AddressEnvelop update(AddressEnvelop address, AddressEnvelop _address) {

		address.setPrimaryContact(_address.getPrimaryContact());
		address.setTelephone1(_address.getTelephone1());
		address.setTelephone2(_address.getTelephone2());
		address.setExt1(_address.getExt1());
		address.setExt2(_address.getExt2());
		address.setTollFree(_address.getTollFree());
		address.setFax(_address.getFax());
		address.setSecondaryContact(_address.getSecondaryContact());
		address.setEmail(_address.getEmail());
		address.setBillingEmail(_address.getBillingEmail());
		address.setMcLinkNumber(_address.getMcLinkNumber());
		address.setAddress(_address.getAddress());
		address.setBillingAddress(_address.getBillingAddress());
		address.setStatus(_address.getStatus());
		
		return addressRepository.save(address);
	}

	public void delete(AddressEnvelop address) {
		addressRepository.delete(address);
	}

	public List<AddressEnvelop> list() {
		return addressRepository.findAll();
	}

	@Override
	public Page<AddressEnvelop> list(int start, int end, String sortBy, String sortType) {
		PageRequest pageRequest = pagingRequest.getPageRequest(start, end, sortBy, sortType);
		return addressRepository.findAll(pageRequest);		
	}
}
