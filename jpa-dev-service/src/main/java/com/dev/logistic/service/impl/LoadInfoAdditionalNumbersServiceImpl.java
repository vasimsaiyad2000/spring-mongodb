package com.dev.logistic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.dev.logistic.entity.LoadInfoAdditionalNumbers;
import com.dev.logistic.repository.LoadInfoAdditionalNumbersRepository;
import com.dev.logistic.service.LoadInfoAdditionalNumbersService;
import com.dev.logistic.service.util.PagingAndSortingRequest;


@Service
public class LoadInfoAdditionalNumbersServiceImpl implements LoadInfoAdditionalNumbersService {

	@Autowired
	private LoadInfoAdditionalNumbersRepository liansRepository;
	
	@Autowired
	private PagingAndSortingRequest pagingRequest;
	
	@Override
	public LoadInfoAdditionalNumbers create(LoadInfoAdditionalNumbers lian) {
		return liansRepository.save(lian);
	}

	@Override
	public LoadInfoAdditionalNumbers find(String id) {
		return liansRepository.findOne(id);
	}

	@Override
	public LoadInfoAdditionalNumbers update(LoadInfoAdditionalNumbers lian, LoadInfoAdditionalNumbers _lian) {
		
		lian.setKey(_lian.getKey());
		lian.setValue(_lian.getValue());
		lian.setShowOnBits(_lian.getShowOnBits());
		
		return liansRepository.save(lian);
	}

	@Override
	public void delete(LoadInfoAdditionalNumbers lian) {
		liansRepository.delete(lian);
	}

	@Override
	public List<LoadInfoAdditionalNumbers> list() {
		return liansRepository.findAll();
	}

	@Override
	public Page<LoadInfoAdditionalNumbers> list(int start, int end, String sortBy, String sortType) {
		PageRequest pageRequest = pagingRequest.getPageRequest(start, end, sortBy, sortType);
		return liansRepository.findAll(pageRequest);
	}

}
