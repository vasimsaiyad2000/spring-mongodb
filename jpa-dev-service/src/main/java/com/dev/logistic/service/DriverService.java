package com.dev.logistic.service;

import com.dev.logistic.entity.Driver;

/**
 * This interface define service layer operations for driver. This inherit
 * BaseService interface to define common CRUD operations.
 * 
 * DriverServiceImpl class implements operations define in this interface.
 * 
 * @author vasim
 * 
 */
public interface DriverService extends BaseService<Driver> {

}
