package com.dev.logistic.service;

import java.util.List;

import com.dev.logistic.constants.LoadStatus;
import com.dev.logistic.entity.LoadInfo;

/**
 * This interface define service layer operations for loadinfo.This inherit
 * BaseService interface to define common CRUD operations. Also define 
 * other operations like search loadinfo by id, search loadinfo by status,
 * search loadinfo by type.
 * 
 * LoadInfoServiceImpl class implements operations for defiend in this interface.
 * 
 * @author vasim
 * 
 */
public interface LoadInfoService extends BaseService<LoadInfo> {

	/**
	 * Search based on loadinfo id
	 * @param id LoadInfo id
	 * @return List of LoadInfo
	 */
	public List<LoadInfo> searchById(String id);
	
	/**
	 * Search based on loadinfo status
	 * @param status LoadInfo status
	 * @return List of LoadInfo
	 */
	public List<LoadInfo> searchByStatus(LoadStatus status);
	
	/**
	 * Search based on loadinfo type
	 * @param type LoadInfo type
	 * @return List of LoadInfo
	 */
	public List<LoadInfo> searchByType(int type);
}
