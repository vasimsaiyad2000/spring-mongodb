package com.dev.logistic.service.util;

import org.apache.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

@Component
public class PagingAndSortingRequest {

	private Logger log = Logger.getLogger(PagingAndSortingRequest.class);
	
	public PageRequest getPageRequest(int start, int end, String sortBy, String sortType){
		
		log.info("Creating paging and sorting request");
		
		if(log.isDebugEnabled()){
			log.debug("Start index of results : " + start);
			log.debug("End index of results : " + end);
		}
		
		int size = end - (start - 1);
		int page = (start - 1)/size;

		log.info("Result page number :" + page + " and result size :" + size);
		log.info("Results are sorted " + sortType + " by field " + sortBy);
		
		return new PageRequest(page, size, new Sort(Direction.ASC, sortBy));
	}
}
