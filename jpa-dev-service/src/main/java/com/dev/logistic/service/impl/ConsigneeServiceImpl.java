package com.dev.logistic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.dev.logistic.entity.Consignee;
import com.dev.logistic.repository.ConsigneeRepository;
import com.dev.logistic.service.ConsigneeService;
import com.dev.logistic.service.util.PagingAndSortingRequest;

@Service
public class ConsigneeServiceImpl implements ConsigneeService {

	@Autowired
	private ConsigneeRepository consigneeRepository;
	
	@Autowired
	private PagingAndSortingRequest pagingRequest;
	
	public Consignee create(Consignee consignee) {
		return consigneeRepository.save(consignee);
	}

	public Consignee find(String id) {
		return consigneeRepository.findOne(id);
	}

	public Consignee update(Consignee consignee, Consignee _consignee) {
		return null;
	}

	public void delete(Consignee consignee) {
		consigneeRepository.delete(consignee);
	}

	public List<Consignee> list() {
		return consigneeRepository.findAll();
	}

	@Override
	public Page<Consignee> list(int start, int end, String sortBy, String sortType) {
		PageRequest pageRequest = pagingRequest.getPageRequest(start, end, sortBy, sortType);
		return consigneeRepository.findAll(pageRequest);
	}
}
