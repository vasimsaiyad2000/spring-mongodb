package com.dev.logistic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.dev.logistic.entity.Invoicee;
import com.dev.logistic.repository.InvoiceeRepository;
import com.dev.logistic.service.InvoiceeService;
import com.dev.logistic.service.util.PagingAndSortingRequest;

@Service
public class InvoiceeServiceImpl implements InvoiceeService {

	@Autowired
	private InvoiceeRepository invoiceeRepository;
	
	@Autowired
	private PagingAndSortingRequest pagingRequest;
	
	public Invoicee create(Invoicee invoicee) {
		return invoiceeRepository.save(invoicee);
	}

	public Invoicee find(String id) {
		return invoiceeRepository.findOne(id);
	}

	public Invoicee update(Invoicee invoicee, Invoicee _invoicee) {
		return null;
	}

	public void delete(Invoicee invoice) {
		invoiceeRepository.delete(invoice);
	}

	public List<Invoicee> list() {
		return invoiceeRepository.findAll();
	}

	@Override
	public Page<Invoicee> list(int start, int end, String sortBy, String sortType) {
		PageRequest pageRequest = pagingRequest.getPageRequest(start, end, sortBy, sortType);
		return invoiceeRepository.findAll(pageRequest);
	}

}
