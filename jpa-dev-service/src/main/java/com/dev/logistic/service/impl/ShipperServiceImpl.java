package com.dev.logistic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.dev.logistic.entity.Shipper;
import com.dev.logistic.repository.ShipperRepository;
import com.dev.logistic.service.ShipperService;
import com.dev.logistic.service.util.PagingAndSortingRequest;

@Service
public class ShipperServiceImpl implements ShipperService {

	@Autowired
	private ShipperRepository shipperRepository;
	
	@Autowired
	private PagingAndSortingRequest pagingRequest;
	
	public Shipper create(Shipper shipper) {
		return shipperRepository.save(shipper);
	}

	public Shipper find(String id) {
		return shipperRepository.findOne(id);
	}

	public Shipper update(Shipper shipper, Shipper _shipper){
	    
	    shipper.setShippingNotes(_shipper.getShippingNotes());
	    shipper.setInternalNotes(_shipper.getInternalNotes());
	    
	    return shipperRepository.save(shipper);
	}

	public void delete(Shipper shipper) {
		shipperRepository.delete(shipper);
	}

	public List<Shipper> list() {
		return shipperRepository.findAll();
	}

	@Override
	public Page<Shipper> list(int start, int end, String sortBy, String sortType) {
		PageRequest pageRequest = pagingRequest.getPageRequest(start, end, sortBy, sortType);
		return shipperRepository.findAll(pageRequest);
	}
}
