package com.dev.logistic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import com.dev.logistic.entity.Vehicle;
import com.dev.logistic.repository.VehicleRepository;
import com.dev.logistic.service.VehicleService;
import com.dev.logistic.service.util.PagingAndSortingRequest;

@Service
public class VehicleServiceImpl implements VehicleService {

	@Autowired
	private VehicleRepository vehicleRepository;
	
	@Autowired
	private PagingAndSortingRequest pagingRequest;
	
	public Vehicle create(Vehicle vehicle) {
		return vehicleRepository.save(vehicle);
	}

	public Vehicle find(String id) {
		return vehicleRepository.findOne(id);
	}

	public Vehicle update(Vehicle vehicle, Vehicle _vehicle) {
		vehicle.setType(_vehicle.getType());
		return vehicleRepository.save(vehicle);
	}

	public void delete(Vehicle vehicle) {
		vehicleRepository.delete(vehicle);
	}

	public List<Vehicle> list() {
		return vehicleRepository.findAll();
	}

	public Page<Vehicle> list(int start, int end, String sortBy, String sortType) {
		PageRequest pageRequest = pagingRequest.getPageRequest(start, end, sortBy, sortType);
		return vehicleRepository.findAll(pageRequest);
	}
}
