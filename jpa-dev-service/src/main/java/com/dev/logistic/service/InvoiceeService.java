package com.dev.logistic.service;

import com.dev.logistic.entity.Invoicee;

/**
 * This interface define service layer operations for invoicee.This inherit
 * BaseService interface to define common CRUD operations.
 * 
 * InvoiceeServiceImpl class implements operations define in this interface.
 * 
 * @author vasim
 * 
 */
public interface InvoiceeService extends BaseService<Invoicee> {

}
