package com.dev.logistic.service;

import java.util.List;

import org.springframework.data.domain.Page;

/**
 * Generic interface that define basic CRUD operations for service layer. This interface
 * can be inherited by other service interface to perform CRUD operations in their
 * implemented class. The inherited interface must provide type to be able to perform 
 * generic CRUD operations.
 * 
 * <p>
 * This interface define below CRUD operations.
 * 
 * 1. Create 2. Find 3. Update 4. Delete 5.List 
 * 
 * <p>
 *   
 * @author vasim
 *
 * @param <T> Type
 */
public interface BaseService<T> {
	
	/**
	 * Perform create operation
	 * @param entity Given entity object 
	 * @return Saved entity object
	 */
	public T create(T entity);
	
	/**
	 * Perform single row find operation
	 * @param id Id field of entity object 
	 * @return Found entity object
	 */
	public T find(String id);
	
	/**
	 * Perform update operation
	 * @param entity Given destination entity object
	 * @param entity Given source entity object
	 * @return Updated entity object
	 */
	public T update(T entity, T _entity);
	
	/**
	 * Perform delete operation
	 * @param entity Given entity object
	 */
	public void delete(T entity);
	
	/**
	 * Perform multiple rows find operation
	 * @return List of entity objects
	 */
	public List<T> list();
	
	/**
	 * Perform multiple rows find operation with paging and sorting based on given values
	 * @param start Start index of results
	 * @param end End index of results
	 * @param sortBy Name of field on which sorting will be applied
	 * @param sortType Type of sorting to perform (ascending, descending)
	 * @return Page object that holds results, paging and sorting details
	 */
	public Page<T> list(int start, int end, String sortBy, String sortType);

}
