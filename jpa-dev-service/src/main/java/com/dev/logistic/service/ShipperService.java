package com.dev.logistic.service;

import com.dev.logistic.entity.Shipper;

public interface ShipperService extends BaseService<Shipper> {

}
