package com.dev.logistic.service;

import com.dev.logistic.entity.AddressEnvelop;

/**
 * This interface define service layer operations for AddressEnvelop. This inherit
 * BaseService interface to define common CRUD operations.
 * 
 * AddressEnvelopServiceImpl class implements operations define in this interface.
 * 
 * @author vasim
 * 
 */
public interface AddressEnvelopService extends BaseService<AddressEnvelop> {

}
