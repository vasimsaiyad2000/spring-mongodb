package com.dev.logistic.service;

import com.dev.logistic.entity.BillofLanding;

/**
 * This interface define service layer operations for BillofLanding. This inherit
 * BaseService interface to define common CRUD operations.
 * 
 * BillofLandingServiceImpl class implements operations define in this interface.
 * 
 * @author vasim
 * 
 */
public interface BillofLandingService extends BaseService<BillofLanding> {

}
