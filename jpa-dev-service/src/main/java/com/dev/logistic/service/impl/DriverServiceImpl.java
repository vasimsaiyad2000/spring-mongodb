package com.dev.logistic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.dev.logistic.entity.Driver;
import com.dev.logistic.repository.DriverRepository;
import com.dev.logistic.service.DriverService;
import com.dev.logistic.service.util.PagingAndSortingRequest;

@Service
public class DriverServiceImpl implements DriverService {

	@Autowired
	private DriverRepository driverRepository;
	
	@Autowired
	private PagingAndSortingRequest pagingRequest;
	
	public Driver create(Driver driver) {
		return driverRepository.save(driver);
	}

	public Driver find(String id) {
		return driverRepository.findOne(id);
	}

	public Driver update(Driver driver, Driver _driver) {
		return null;
	}

	public void delete(Driver driver) {
		driverRepository.delete(driver);
	}

	public List<Driver> list() {
		return driverRepository.findAll();
	}

	@Override
	public Page<Driver> list(int start, int end, String sortBy, String sortType) {
		PageRequest pageRequest = pagingRequest.getPageRequest(start, end, sortBy, sortType);
		return driverRepository.findAll(pageRequest);
	}

}
