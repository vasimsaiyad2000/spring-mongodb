package com.dev.logistic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.dev.logistic.constants.LoadStatus;
import com.dev.logistic.entity.LoadInfo;
import com.dev.logistic.repository.LoadInfoRepository;
import com.dev.logistic.service.LoadInfoService;
import com.dev.logistic.service.util.PagingAndSortingRequest;

@Service
public class LoadInfoServiceImpl implements LoadInfoService {

	@Autowired
	private LoadInfoRepository loadInfoRepository;
	
	@Autowired
	private PagingAndSortingRequest pagingRequest;
	
	public LoadInfo create(LoadInfo loadInfo) {
		loadInfoRepository.save(loadInfo);
		return loadInfoRepository.findOne(loadInfo.getId());
	}

	public LoadInfo find(String id) {
		return loadInfoRepository.findOne(id);
	}

	public LoadInfo update(LoadInfo loadInfo, LoadInfo _loadInfo) {
		
		loadInfo.setUsername(_loadInfo.getUsername());
		loadInfo.setSalesRepid(_loadInfo.getSalesRepid());
		loadInfo.setDispatcherId(_loadInfo.getDispatcherId());
		loadInfo.setStatus(_loadInfo.getStatus());
		loadInfo.setWorkOrder(_loadInfo.getWorkOrder());
		loadInfo.setType(_loadInfo.getType());
		loadInfo.setRate(_loadInfo.getRate());
		loadInfo.setNoOfUnits(_loadInfo.getNoOfUnits());
		loadInfo.setP_by_d(_loadInfo.getP_by_d());
		loadInfo.setF_s_c(_loadInfo.getF_s_c());
		loadInfo.setOtherCharges(_loadInfo.getOtherCharges());
		loadInfo.setTotalRate(_loadInfo.getTotalRate());
		loadInfo.setEquipmentId(_loadInfo.getEquipmentId());
		loadInfo.setDirverFlatRate(_loadInfo.getDirverFlatRate());
		loadInfo.setProratedMiles(_loadInfo.getProratedMiles());
		loadInfo.setProratedEmptyMiles(_loadInfo.getProratedEmptyMiles());
		loadInfo.setDirverMiles(_loadInfo.getDirverMiles());
		loadInfo.setDirverEmptyMiles(_loadInfo.getDirverEmptyMiles());
		loadInfo.setDriverHourly(_loadInfo.getDriverHourly());
		loadInfo.setDriver(_loadInfo.getDriver());
		loadInfo.setCarrier(_loadInfo.getCarrier());
		loadInfo.setInvoicee(_loadInfo.getInvoicee());
		loadInfo.setConsignees(_loadInfo.getConsignees());
		loadInfo.setShippers(_loadInfo.getShippers());
		loadInfo.setVehicles(_loadInfo.getVehicles());
		loadInfo.setLoadinfoAddtionalNumbers(_loadInfo.getLoadinfoAddtionalNumbers());
		loadInfo.setBillofLandings(_loadInfo.getBillofLandings());
		
		loadInfoRepository.save(loadInfo);
		return loadInfoRepository.findOne(loadInfo.getId());
	}

	public void delete(LoadInfo loadInfo) {
		loadInfoRepository.delete(loadInfo);
	}

	public List<LoadInfo> list() {
		return loadInfoRepository.findAll();
	}

	public List<LoadInfo> searchById(String id) {
		return loadInfoRepository.findById(id);
	}

	public List<LoadInfo> searchByStatus(LoadStatus status) {
		return loadInfoRepository.findByStatus(status);
	}

	public List<LoadInfo> searchByType(int type) {
		return loadInfoRepository.findByType(type);
	}

	@Override
	public Page<LoadInfo> list(int start, int end, String sortBy, String sortType) {
		PageRequest pageRequest = pagingRequest.getPageRequest(start, end, sortBy, sortType);
		return loadInfoRepository.findAll(pageRequest);
	}
}
