package com.dev.logistic.service;


import com.dev.logistic.entity.Vehicle;

/**
 * This interface define service layer operations for vehicle.This inherit
 * BaseService interface to define common CRUD operations.
 * 
 * VehicleServiceImpl class implements all operation for this interface.
 * 
 * @author vasim
 * 
 */
public interface VehicleService extends BaseService<Vehicle> {

}
