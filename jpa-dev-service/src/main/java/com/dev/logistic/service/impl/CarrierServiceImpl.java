package com.dev.logistic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.dev.logistic.entity.Carrier;
import com.dev.logistic.repository.CarrierRepository;
import com.dev.logistic.service.CarrierService;
import com.dev.logistic.service.util.PagingAndSortingRequest;

@Service
public class CarrierServiceImpl implements CarrierService {

	@Autowired
	private CarrierRepository carrierRepository;
	
	@Autowired
	private PagingAndSortingRequest pagingRequest;
	
	public Carrier create(Carrier carrier) {
		return carrierRepository.save(carrier);
	}

	public Carrier find(String id) {
		return carrierRepository.findOne(id);
	}

	public Carrier update(Carrier carrier, Carrier _carrier) {
		carrier.setReceivingNotes(_carrier.getReceivingNotes());
		carrier.setInternalNotes(_carrier.getInternalNotes());
		
		return carrierRepository.save(carrier);
		
	}

	public void delete(Carrier carrier) {
		carrierRepository.delete(carrier);
	}

	public List<Carrier> list() {
		return carrierRepository.findAll();
	}

	@Override
	public Page<Carrier> list(int start, int end, String sortBy, String sortType) {
		PageRequest pageRequest = pagingRequest.getPageRequest(start, end, sortBy, sortType);
		return carrierRepository.findAll(pageRequest);
	}
}
