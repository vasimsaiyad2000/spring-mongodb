package com.dev.logistic.service;

import com.dev.logistic.entity.Carrier;

/**
 * This interface define service layer operations for Carrier.This inherit
 * BaseService interface to define common CRUD operations.
 * 
 * CarrierServiceImpl class implements operations define in this interface.
 * 
 * @author vasim
 * 
 */
public interface CarrierService extends BaseService<Carrier> {

}
