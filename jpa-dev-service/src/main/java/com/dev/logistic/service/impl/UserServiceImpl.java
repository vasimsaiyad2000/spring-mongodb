package com.dev.logistic.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.dev.logistic.entity.User;
import com.dev.logistic.repository.UserRepository;
import com.dev.logistic.service.UserService;
import com.dev.logistic.service.util.PagingAndSortingRequest;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder encoder;
	
	@Autowired
	private PagingAndSortingRequest pagingRequest;
	
	public User create(User user) {
		user.setPassword(encoder.encode(user.getPassword()));
		
		// Applying default role to user
		List<String> roles = new ArrayList<String>();
		roles.add("ROLE_USER");
		
		user.setRoles(roles);
		return userRepository.save(user);
	}

	public User find(String id) {
		return userRepository.findOne(id);
	}

	public User update(User user, User _user) {
		
		user.setPassword(encoder.encode(_user.getPassword()));
		return userRepository.save(user);
	}

	public void delete(User user) {
		userRepository.delete(user);
	}

	public List<User> list() {
		return userRepository.findAll();
	}

	@Override
	public boolean isUserNameExist(User user) {
		boolean isUserNameExist = false;
		User _user = userRepository.findByUserName(user.getUserName());
		
		if(_user != null && !_user.getId().equals(user.getId())){
			isUserNameExist = true;
		}
		
		return isUserNameExist;
	}

	@Override
	public boolean isEmailExist(User user) {
		boolean isEmailExist = false;
		User _user = userRepository.findByEmail(user.getEmail());
		
		if(_user != null && !_user.getId().equals(user.getId())){
			isEmailExist = true;
		}
		
		return isEmailExist;
	}

	@Override
	public Page<User> list(int start, int end, String sortBy, String sortType) {
		PageRequest pageRequest = pagingRequest.getPageRequest(start, end, sortBy, sortType);
		return userRepository.findAll(pageRequest);
	}
}
