package com.dev.logistic.service;

import com.dev.logistic.entity.Consignee;

/**
 * This interface define service layer operations for Consignee.This inherit
 * BaseService interface to define common CRUD operations.
 * 
 * ConsigneeServiceImpl class implements operations define in this interface.
 * 
 * @author vasim
 * 
 */
public interface ConsigneeService extends BaseService<Consignee> {

}
