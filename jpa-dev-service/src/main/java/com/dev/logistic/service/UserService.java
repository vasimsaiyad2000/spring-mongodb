package com.dev.logistic.service;

import com.dev.logistic.entity.User;
import com.dev.logistic.validation.component.FieldValueExists;

/**
 * This interface define service layer operations for user.This inherit
 * BaseService interface to define common CRUD operations.
 * 
 * UserServiceImpl class implements operations for this interface.
 * 
 * @author vasim
 * 
 */
public interface UserService extends BaseService<User> {

	/**
	 * Check wheather username is exist or not.
	 * @param user User object
	 * @return True if exist else False
	 */
	public boolean isUserNameExist(User user);
	
	/**
	 * Check wheather email is exist or not
	 * @param user User object
	 * @return True if exist else False
	 */
	public boolean isEmailExist(User user);
}
