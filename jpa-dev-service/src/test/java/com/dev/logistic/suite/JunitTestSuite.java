package com.dev.logistic.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.dev.logistic.service.AddressServiceTest;
import com.dev.logistic.service.BillofLandingServiceTest;
import com.dev.logistic.service.CarrierServiceTest;
import com.dev.logistic.service.ConsigneeServiceTest;
import com.dev.logistic.service.DriverServiceTest;
import com.dev.logistic.service.InvoiceeServiceTest;
import com.dev.logistic.service.LoadInfoAdditionalNumbersServiceTest;
import com.dev.logistic.service.LoadinfoServiceTest;
import com.dev.logistic.service.ShipperServiceTest;
import com.dev.logistic.service.UserServiceTest;
import com.dev.logistic.service.VehicleServiceTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	AddressServiceTest.class,
	BillofLandingServiceTest.class,
	CarrierServiceTest.class,
	ConsigneeServiceTest.class,
	DriverServiceTest.class,
	InvoiceeServiceTest.class,
	LoadInfoAdditionalNumbersServiceTest.class,
	UserServiceTest.class,
	VehicleServiceTest.class,
	ShipperServiceTest.class,
	LoadinfoServiceTest.class
	
})
public class JunitTestSuite {

}
