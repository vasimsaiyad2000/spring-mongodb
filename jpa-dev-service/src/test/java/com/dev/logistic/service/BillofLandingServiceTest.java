package com.dev.logistic.service;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.dev.logistic.constants.CustomerStatus;
import com.dev.logistic.entity.Address;
import com.dev.logistic.entity.AddressEnvelop;
import com.dev.logistic.entity.BillofLanding;

public class BillofLandingServiceTest extends BaseServiceTest{

	@Autowired
	private BillofLandingService bolService;
	
	@Autowired
	private AddressEnvelopService addressEnvelopeService;
	
	private Address createNewAddress(){
		Address addr = new Address();
		
		addr.setAddress("address");
		addr.setAddress1("address1");
		addr.setAddress2("address2");
		addr.setCity("city");
		addr.setCountrycode("countrycode");
		addr.setStatecode("statecode");
		addr.setZip("349089");
		
		return addr;
	}
	
	private Address createNewBillingAddress(){
		Address billingAddr = new Address();
		billingAddr.setAddress("address");
		billingAddr.setAddress1("address1");
		billingAddr.setAddress2("address2");
		billingAddr.setCity("city");
		billingAddr.setCountrycode("countrycode");
		billingAddr.setStatecode("statecode");
		billingAddr.setZip("349089");
		
		return billingAddr;
	}
	
	private AddressEnvelop createNewAddressEnvelpe(){
		
		AddressEnvelop address = new AddressEnvelop();
		address.setEmail("a@a.com");
		address.setBillingEmail("a@a.com");
		address.setExt1("192");
		address.setExt2("198");
		address.setFax("123-345-3333");
		address.setPrimaryContact("Primary Contact");
		address.setTelephone1("123-456-7890");
		address.setTelephone2("123-456-7890");
		address.setStatus("Active");
	
		address.setAddress(createNewAddress());
		address.setBillingAddress(createNewBillingAddress());
		
		return addressEnvelopeService.create(address);
	}
	
	private BillofLanding createNewBOL(){
		BillofLanding bol = new BillofLanding();
		
		bol.setBol("bol");
		bol.setThirdparty("ABC Corp");
		bol.setDriver("John Stephen");
		bol.setOrigin("West");
		bol.setDestination("San Fransisco");
		bol.setEmgergencynum("123-345-5677");
		bol.setCod("xyx");
		bol.setValue("234");
		bol.setNotes("Test Notes");
		bol.setItemPieces("2");
		bol.setItemDesc("Test Description");
		bol.setItemLbs("Test Lbs");
		bol.setItemType("Metal");
		bol.setItemNMFC("itemNMFC");
		bol.setItemClass("Z");
		bol.setItemHM("34");
		
		bol.setAddressEnvelop(createNewAddressEnvelpe());
		
		return bol;
	}
	
	@Override
	@Test
	public void createTest() {
		BillofLanding bol = createNewBOL();
		BillofLanding bolSaved = bolService.create(bol);
		
		assertNotNull(bolSaved);
		assertNotNull(bolSaved.getId());
		
	}

	@Override
	@Test
	public void updateTest() {
		BillofLanding bolSaved = bolService.create(createNewBOL());
		
		BillofLanding bol = createNewBOL();
		bol.setThirdparty("Test Corp");
		bol.setDriver("Test Driver");
		bol.setOrigin("East");
		bol.setDestination("Newyork");
		
		bol = bolService.update(bolSaved, bol);
		
		assertNotNull(bol);
		
	}

	@Override
	@Test
	public void findTest() {
		BillofLanding bolSaved = bolService.create(createNewBOL());
		assertNotNull(bolService.find(bolSaved.getId()));
	}

	@Override
	@Test
	public void listTest() {
		this.createTest();
		List<BillofLanding> list = bolService.list();
		
		assertNotNull(list);
		assertTrue(list.size() > 0);
	}

	@Override
	@Test
	public void deleteTest() {
		BillofLanding bolSaved = bolService.create(createNewBOL());
		bolService.delete(bolSaved);
		
		assertNull(bolService.find(bolSaved.getId()));
		
	}
}
