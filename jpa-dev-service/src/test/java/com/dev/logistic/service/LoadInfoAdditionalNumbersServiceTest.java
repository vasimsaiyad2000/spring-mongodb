package com.dev.logistic.service;


import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.dev.logistic.entity.LoadInfoAdditionalNumbers;


public class LoadInfoAdditionalNumbersServiceTest extends BaseServiceTest {

	@Autowired
	private LoadInfoAdditionalNumbersService lianService;
	
	private LoadInfoAdditionalNumbers createNewLian(){
	  LoadInfoAdditionalNumbers lian = new LoadInfoAdditionalNumbers();
	  
	  lian.setKey("Test Key");
	  lian.setValue("Test Value");
	  lian.setShowOnBits((byte) 128);
	  
	  return lian;
	}
	
	@Override
	@Test
	public void createTest() {
		LoadInfoAdditionalNumbers lian = createNewLian();
		LoadInfoAdditionalNumbers lianSaved = lianService.create(lian);
		
		assertNotNull(lianSaved);
		assertNotNull(lianSaved.getId());
		
	}

	@Override
	@Test
	public void updateTest() {
		LoadInfoAdditionalNumbers lianSaved = lianService.create(createNewLian());
		LoadInfoAdditionalNumbers lian = createNewLian();
		
		lian.setKey("Test Key");
		lian.setValue("Test Value");
		lian.setShowOnBits((byte) 64);
		
		lianService.update(lianSaved, lian);
		assertNotNull(lian);
		
	}

	@Override
	@Test
	public void findTest() {
		LoadInfoAdditionalNumbers lianSaved = lianService.create(createNewLian());
		assertNotNull(lianService.find(lianSaved.getId()));
		
	}

	@Override
	@Test
	public void listTest() {
		this.createTest();
		List<LoadInfoAdditionalNumbers> list = lianService.list();
		
		assertNotNull(list);
		assertTrue(list.size() > 0);
		
	}

	@Override
	@Test
	public void deleteTest() {
		LoadInfoAdditionalNumbers lianSaved = lianService.create(createNewLian());
		lianService.delete(lianSaved);
		
		assertNull(lianService.find(lianSaved.getId()));
		
	}

}
