package com.dev.logistic.service;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(value = "classpath:application-service.xml")
public abstract class BaseServiceTest {
	
	public abstract void createTest();
	public abstract void updateTest();
	public abstract void findTest();
	public abstract void listTest();
	public abstract void deleteTest();

}
