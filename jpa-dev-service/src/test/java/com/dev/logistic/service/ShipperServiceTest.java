package com.dev.logistic.service;


import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.dev.logistic.constants.CustomerStatus;
import com.dev.logistic.entity.Address;
import com.dev.logistic.entity.AddressEnvelop;
import com.dev.logistic.entity.Shipper;


public class ShipperServiceTest extends BaseServiceTest {

	@Autowired
	private ShipperService shipperService;
	
	@Autowired
	private AddressEnvelopService addressEnvelopeService;
	
	private Address createNewAddress(){
		Address addr = new Address();
		
		addr.setAddress("address");
		addr.setAddress1("address1");
		addr.setAddress2("address2");
		addr.setCity("city");
		addr.setCountrycode("countrycode");
		addr.setStatecode("statecode");
		addr.setZip("349089");
		
		return addr;
	}
	
	private Address createNewBillingAddress(){
		Address billingAddr = new Address();
		billingAddr.setAddress("address");
		billingAddr.setAddress1("address1");
		billingAddr.setAddress2("address2");
		billingAddr.setCity("city");
		billingAddr.setCountrycode("countrycode");
		billingAddr.setStatecode("statecode");
		billingAddr.setZip("349089");
		
		return billingAddr;
	}
	
	private AddressEnvelop createNewAddressEnvelpe(){
		
		AddressEnvelop address = new AddressEnvelop();
		address.setEmail("a@a.com");
		address.setBillingEmail("a@a.com");
		address.setExt1("192");
		address.setExt2("198");
		address.setFax("123-345-3333");
		address.setPrimaryContact("Primary Contact");
		address.setTelephone1("123-456-7890");
		address.setTelephone2("123-456-7890");
		address.setStatus("Active");
	
		address.setAddress(createNewAddress());
		address.setBillingAddress(createNewBillingAddress());
		
		return addressEnvelopeService.create(address);
	}
	
	private Shipper createNewShipper(){
		
		Shipper shipper = new Shipper();
		shipper.setShippingNotes("Shipping notes");
		shipper.setInternalNotes("Internal notes");
		
		AddressEnvelop address = createNewAddressEnvelpe();
		shipper.setAddressEnvelop(address);

		return shipper;
		
	}
	
	@Override
	@Test
	public void createTest() {
		
		Shipper shipper = createNewShipper();
		Shipper shipperSaved = shipperService.create(shipper);
		
		assertNotNull(shipperSaved);
		assertNotNull(shipperSaved.getId());

	}

	@Override
	@Test
	public void updateTest() {
		Shipper shipperSaved = shipperService.create(createNewShipper());
		
		Shipper shipper = createNewShipper();
		shipper.setShippingNotes("This is Shipping notes");
		shipper.setInternalNotes("This is internal notes");
		
		shipper = shipperService.update(shipperSaved, shipper);
		assertNotNull(shipper);
		
	}

	@Override
	@Test
	public void findTest() {
		Shipper shipperSaved = shipperService.create(createNewShipper());
		assertNotNull(shipperService.find(shipperSaved.getId()));
	}

	@Override
	@Test
	public void listTest() {
		this.createTest();
		List<Shipper> list = shipperService.list();
		
		assertNotNull(list);
		assertTrue(list.size() > 0);

	}

	@Override
	@Test
	public void deleteTest() {
		
		Shipper shipperSaved = shipperService.create(createNewShipper());
		shipperService.delete(shipperSaved);
	
		Shipper shipperDeleted = shipperService.find(shipperSaved.getId());
		assertNull(shipperDeleted);

	}
}
