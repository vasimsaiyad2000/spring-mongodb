package com.dev.logistic.service;

import static junit.framework.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.dev.logistic.entity.User;

public class UserServiceTest extends BaseServiceTest{

	@Autowired
	private UserService userService;
	

	private User createNewUser(){
		User user = new User();
		
		user.setFirstName("Admin");
		user.setLastName("Admin");
		user.setEmail("admin@admin.com");
		user.setUserName("admin");
		user.setPassword("admin");
		
		return user;
	}
	
	@Override
	@Test
	public void createTest() {
		User user = createNewUser();
		User userSaved = userService.create(user);
		
		assertNotNull(userSaved);
		assertNotNull(userSaved.getId());
	}

	@Override
	@Test
	public void updateTest() {
		User userSaved = userService.create(createNewUser());
		User user = createNewUser();
		
		user.setFirstName("Admin FirstName");
		user.setLastName("Admin LastName");
		
		user = userService.update(userSaved, user);
		assertNotNull(user);
		
	}

	@Override
	@Test
	public void findTest() {
		User userSaved = userService.create(createNewUser());
		assertNotNull(userService.find(userSaved.getId()));
		
	}

	@Override
	@Test
	public void listTest() {
		this.createTest();
		List<User> list = userService.list();
		
		assertNotNull(list);
		assertTrue(list.size() > 0);
		
	}

	@Override
	@Test
	public void deleteTest() {
		User userSaved = userService.create(createNewUser());
		userService.delete(userSaved);
		
		User userDeleted = userService.find(userSaved.getId());
		assertNull(userDeleted);
		
	}
	
	@Test
	public void uniqueCheckTest(){
		User userSaved = createNewUser();
		
		boolean isEmailExist = userService.isEmailExist(userSaved);
		boolean isUserNameExist = userService.isUserNameExist(userSaved);
		
		assertEquals(true, isEmailExist);
		assertEquals(true, isUserNameExist);
	}
}
