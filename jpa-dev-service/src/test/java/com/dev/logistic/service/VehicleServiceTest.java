package com.dev.logistic.service;

import static junit.framework.Assert.*;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.dev.logistic.entity.Vehicle;

public class VehicleServiceTest extends BaseServiceTest{

	@Autowired
	private VehicleService vehicleService;
	
	private Vehicle createNewVehicle() {
		
		  Vehicle vehicle = new Vehicle();
		  vehicle.setType("Heavy");
		  return vehicle;
	}
	
	@Override
	@Test
	public void createTest() {
		Vehicle vehicle = createNewVehicle();
		Vehicle vehicleSaved = vehicleService.create(vehicle);
		
		assertNotNull(vehicleSaved);
		assertNotNull(vehicleSaved.getId());
	}

	@Override
	@Test
	public void updateTest() {
		Vehicle vehicleSaved = vehicleService.create(createNewVehicle());
		
		Vehicle vehicle = createNewVehicle();
		vehicle.setType("Heavy1");
		vehicle = vehicleService.update(vehicleSaved, vehicle);
		
		assertNotNull(vehicle);
		assertEquals("Heavy1", vehicleService.find(vehicle.getId()).getType());
		
	}

	@Override
	@Test
	public void findTest() {
		Vehicle vehicleSaved = vehicleService.create(createNewVehicle());
		assertNotNull(vehicleService.find(vehicleSaved.getId()));
	}

	@Override
	@Test
	public void listTest() {
		this.createTest();
		List<Vehicle> list = vehicleService.list();

		assertNotNull(list);
		assertTrue(list.size() > 0);
	}
	
	@Override
	@Test
	public void deleteTest() {
		
		Vehicle vehicle = createNewVehicle();
		Vehicle vehicleSaved = vehicleService.create(vehicle);
	
		vehicleService.delete(vehicleSaved);
		Vehicle vehicleDeleted = vehicleService.find(vehicleSaved.getId());
		
		assertNull(vehicleDeleted);
		
	}
}
