package com.dev.logistic.service;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.dev.logistic.constants.CustomerStatus;
import com.dev.logistic.entity.Address;
import com.dev.logistic.entity.AddressEnvelop;
import com.dev.logistic.entity.Carrier;


public class CarrierServiceTest extends BaseServiceTest{

	@Autowired
	private CarrierService carrierService;
	
	@Autowired
	private AddressEnvelopService addressEnvelopeService;
	
	private Address createNewAddress(){
		Address addr = new Address();
		
		addr.setAddress("address");
		addr.setAddress1("address1");
		addr.setAddress2("address2");
		addr.setCity("city");
		addr.setCountrycode("countrycode");
		addr.setStatecode("statecode");
		addr.setZip("349089");
		
		return addr;
	}
	
	private Address createNewBillingAddress(){
		Address billingAddr = new Address();
		
		billingAddr.setAddress("address");
		billingAddr.setAddress1("address1");
		billingAddr.setAddress2("address2");
		billingAddr.setCity("city");
		billingAddr.setCountrycode("countrycode");
		billingAddr.setStatecode("statecode");
		billingAddr.setZip("349089");
		
		return billingAddr;
	}
	
	private AddressEnvelop createNewAddressEnvelpe(){
		AddressEnvelop address = new AddressEnvelop();
		
		address.setEmail("a@a.com");
		address.setBillingEmail("a@a.com");
		address.setExt1("192");
		address.setExt2("198");
		address.setFax("123-345-3333");
		address.setPrimaryContact("Primary Contact");
		address.setTelephone1("123-456-7890");
		address.setTelephone2("123-456-7890");
		address.setStatus("Active");
	
		address.setAddress(createNewAddress());
		address.setBillingAddress(createNewBillingAddress());
		
		return addressEnvelopeService.create(address);
	}
	
	private Carrier createNewCarrier(){
		
		Carrier carrier = new Carrier();
		carrier.setReceivingNotes("Receiving notes");
		carrier.setInternalNotes("Internal notes");
		
		AddressEnvelop address = createNewAddressEnvelpe();
		carrier.setAddressEnvelop(address);

		return carrier;
		
	}
	
	@Override
	@Test
	public void createTest() {
		
		Carrier carrier = createNewCarrier();
		Carrier carrierSaved = carrierService.create(carrier);
		
		assertNotNull(carrierSaved);
		assertNotNull(carrierSaved.getId());

	}

	@Override
	@Test
	public void updateTest() {
		Carrier carrierSaved = carrierService.create(createNewCarrier());
		
		Carrier carrier = createNewCarrier();
		carrier.setReceivingNotes("This is receiving notes");
		carrier.setInternalNotes("This is internal notes");
		
		carrier = carrierService.update(carrierSaved, carrier);
		assertNotNull(carrier);
		
	}

	@Override
	@Test
	public void findTest() {
		Carrier carrierSaved = carrierService.create(createNewCarrier());
		assertNotNull(carrierService.find(carrierSaved.getId()));
	}

	@Override
	@Test
	public void listTest() {
		this.createTest();
		List<Carrier> list = carrierService.list();
		
		assertNotNull(list);
		assertTrue(list.size() > 0);

	}

	@Override
	@Test
	public void deleteTest() {
		Carrier carrierSaved = carrierService.create(createNewCarrier());
		
		carrierService.delete(carrierSaved);
		assertNull(carrierService.find(carrierSaved.getId()));
	}
}
