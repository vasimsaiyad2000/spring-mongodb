package com.dev.logistic.service;

import static junit.framework.Assert.*;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.dev.logistic.constants.CustomerStatus;
import com.dev.logistic.entity.Address;
import com.dev.logistic.entity.AddressEnvelop;
import com.dev.logistic.entity.Invoicee;

public class InvoiceeServiceTest extends BaseServiceTest{

	@Autowired
	private InvoiceeService invoiceeService;
	
	@Autowired
	private AddressEnvelopService addressService;
	
	private Address createNewAddress(){
		Address addr = new Address();
		
		addr.setAddress("address");
		addr.setAddress1("address1");
		addr.setAddress2("address2");
		addr.setCity("city");
		addr.setCountrycode("countrycode");
		addr.setStatecode("statecode");
		addr.setZip("349089");
		
		return addr;
	}
	
	private Address createNewBillingAddress(){
		Address billingAddr = new Address();
		billingAddr.setAddress("address");
		billingAddr.setAddress1("address1");
		billingAddr.setAddress2("address2");
		billingAddr.setCity("city");
		billingAddr.setCountrycode("countrycode");
		billingAddr.setStatecode("statecode");
		billingAddr.setZip("349089");
		
		return billingAddr;
	}
	
	private AddressEnvelop createNewAddressEnvelpe(){
		
		AddressEnvelop address = new AddressEnvelop();
		address.setEmail("a@a.com");
		address.setBillingEmail("a@a.com");
		address.setExt1("192");
		address.setExt2("198");
		address.setFax("123-345-3333");
		address.setPrimaryContact("Primary Contact");
		address.setTelephone1("123-456-7890");
		address.setTelephone2("123-456-7890");
		address.setStatus("Active");
	
		address.setAddress(createNewAddress());
		address.setBillingAddress(createNewBillingAddress());
		
		return addressService.create(address);
	}
	
	private Invoicee createNewInvoicee(){
		
		Invoicee invoicee = new Invoicee();
	
		AddressEnvelop address = createNewAddressEnvelpe();
		invoicee.setAddressEnvelop(address);

		return invoicee;
		
	}
	
	@Override
	@Test
	public void createTest() {
		Invoicee invoicee = createNewInvoicee();
		Invoicee invoiceeSaved = invoiceeService.create(invoicee);
		
		assertNotNull(invoiceeSaved);
		assertNotNull(invoiceeSaved.getId());
	}

	@Override
	@Test
	public void updateTest() {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Test
	public void findTest() {
		Invoicee invoiceeSaved = invoiceeService.create(createNewInvoicee());
		assertNotNull(invoiceeService.find(invoiceeSaved.getId()));
		
	}

	@Override
	@Test
	public void listTest() {
		this.createTest();
		List<Invoicee> list = invoiceeService.list();
		
		assertNotNull(list);
		assertTrue(list.size() > 0);
		
	}

	@Override
	@Test
	public void deleteTest() {
		Invoicee invoiceeSaved = invoiceeService.create(createNewInvoicee());
		invoiceeService.delete(invoiceeSaved);

		assertNull(invoiceeService.find(invoiceeSaved.getId()));
	}
}
