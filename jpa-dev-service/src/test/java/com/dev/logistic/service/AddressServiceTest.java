package com.dev.logistic.service;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.dev.logistic.constants.CustomerStatus;
import com.dev.logistic.entity.Address;
import com.dev.logistic.entity.AddressEnvelop;

public class AddressServiceTest extends BaseServiceTest{

	@Autowired
	AddressEnvelopService addressService;
	
	private Address createNewAddress(){
		Address addr = new Address();
		
		addr.setAddress("address");
		addr.setAddress1("address1");
		addr.setAddress2("address2");
		addr.setCity("city");
		addr.setCountrycode("countrycode");
		addr.setStatecode("statecode");
		addr.setZip("349089");
		
		return addr;
	}
	
	private Address createNewBillingAddress(){
		Address billingAddr = new Address();

		billingAddr.setAddress("address");
		billingAddr.setAddress1("address1");
		billingAddr.setAddress2("address2");
		billingAddr.setCity("city");
		billingAddr.setCountrycode("countrycode");
		billingAddr.setStatecode("statecode");
		billingAddr.setZip("349089");
		
		return billingAddr;
	}
	
	private AddressEnvelop createNewAddressEnvelpe(){
		AddressEnvelop address = new AddressEnvelop();
		
		address.setEmail("a@a.com");
		address.setBillingEmail("a@a.com");
		address.setExt1("192");
		address.setExt2("198");
		address.setFax("123-345-3333");
		address.setPrimaryContact("Primary Contact");
		address.setTelephone1("123-456-7890");
		address.setTelephone2("123-456-7890");
		address.setStatus("Active");
	
		address.setAddress(createNewAddress());
		address.setBillingAddress(createNewBillingAddress());
		
		return address;
	}
	
	@Override
	@Test
	public void createTest() {
		AddressEnvelop addressEnvelop = createNewAddressEnvelpe();
		AddressEnvelop addressEnvelopSaved = addressService.create(addressEnvelop);
		
		assertNotNull(addressEnvelopSaved);
		assertNotNull(addressEnvelopSaved.getId());
		
	}

	@Override
	@Test
	public void updateTest() {
		AddressEnvelop addressEnvelopSaved = addressService.create(createNewAddressEnvelpe());
		AddressEnvelop address = createNewAddressEnvelpe();
	
		address.setEmail("a@b.com");
		address.setBillingEmail("a@b.com");
		address.setExt1("194");
		address.setExt2("201");
		address.setFax("123-345-2333");
		address.setPrimaryContact("Primary Contact");
		address.setTelephone1("123-434-7890");
		address.setTelephone2("123-456-7890");
		address.setStatus("Prospect Active");
	
		address.getAddress().setAddress1("Test Address1");
		address.getAddress().setCity("Test City");
		address.getAddress().setCountrycode("IND");
		address.getAddress().setStatecode("GJ");
		address.getAddress().setZip("380055");
		
		address.getBillingAddress().setAddress1("Test Billing Address1");
		address.getBillingAddress().setCity("Test Billing City");
		address.getBillingAddress().setCountrycode("IND");
		address.getBillingAddress().setStatecode("GJ");
		address.getBillingAddress().setZip("380055");
		
		address = addressService.update(addressEnvelopSaved, address);
		assertNotNull(address);
	}

	@Override
	@Test
	public void findTest() {
		AddressEnvelop addressSaved = addressService.create(createNewAddressEnvelpe());
		assertNotNull(addressService.find(addressSaved.getId()));
		
	}

	@Override
	@Test
	public void listTest() {
		this.createTest();
		List<AddressEnvelop> list = addressService.list();
		
		assertNotNull(list);
		assertTrue(list.size() > 0);
		
	}

	@Override
	@Test
	public void deleteTest() {
		AddressEnvelop addressSaved = addressService.create(createNewAddressEnvelpe());
		addressService.delete(addressSaved);
		
		AddressEnvelop addressDeleted = addressService.find(addressSaved.getId());
		assertNull(addressDeleted);
				
	}
}
