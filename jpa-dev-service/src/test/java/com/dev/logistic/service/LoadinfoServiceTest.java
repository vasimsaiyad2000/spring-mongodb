package com.dev.logistic.service;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.dev.logistic.constants.LoadStatus;
import com.dev.logistic.entity.BillofLanding;
import com.dev.logistic.entity.Carrier;
import com.dev.logistic.entity.Consignee;
import com.dev.logistic.entity.Driver;
import com.dev.logistic.entity.Invoicee;
import com.dev.logistic.entity.LoadInfo;
import com.dev.logistic.entity.LoadInfoAdditionalNumbers;
import com.dev.logistic.entity.Shipper;
import com.dev.logistic.entity.Vehicle;

public class LoadinfoServiceTest extends BaseServiceTest{

	@Autowired
	private LoadInfoService loadinfoService;
	
	private LoadInfo createNewLoadInfo(){
		LoadInfo loadInfo = new LoadInfo();
		
		loadInfo.setUsername("admin");
		loadInfo.setSalesRepid("SL1340999991");
		loadInfo.setDispatcherId("DI1093450");
		loadInfo.setStatus(LoadStatus.Open);
		loadInfo.setWorkOrder(1);
		loadInfo.setType(1);
		loadInfo.setRate(10);
		loadInfo.setNoOfUnits(40);
		loadInfo.setP_by_d(4);
		loadInfo.setF_s_c(2);
		loadInfo.setOtherCharges(25);
		loadInfo.setTotalRate(80);
		loadInfo.setEquipmentId("E5689644");
		loadInfo.setDirverFlatRate(15);
		loadInfo.setProratedMiles(50);
		loadInfo.setProratedEmptyMiles(30);
		loadInfo.setDirverMiles(20);
		loadInfo.setDirverEmptyMiles(15);
		loadInfo.setDriverHourly(30);
		loadInfo.setDriver(new Driver("55d4537644ae 4659aa154799"));
		loadInfo.setCarrier(new Carrier("55d4541a44ae4b26fd13bee9"));
		loadInfo.setInvoicee(new Invoicee("55d4544e44ae8b4dd790fea8"));
	
		// Adding shippers
		List<Shipper> shippers = new ArrayList<Shipper>();
		shippers.add(new Shipper("55ed769d44aefd3e88bb9f60"));
		shippers.add(new Shipper("55f2715b44ae7da4c76fe2cb"));
				
		loadInfo.setShippers(shippers);
		
		// Adding consignees
		List<Consignee> consignees = new ArrayList<Consignee>();
		consignees.add(new Consignee("55d454aa44aea95533a1d4f1"));
		consignees.add(new Consignee("55f2819844ae1dc44684a0f1"));
		
		loadInfo.setConsignees(consignees);
		
		// Adding vehicles
		List<Vehicle> vehicles = new ArrayList<Vehicle>();
		vehicles.add(new Vehicle("55e8339a44ae4f0e5e48e2ed"));
		vehicles.add(new Vehicle("55e833e244ae6008f45dbb9a"));
		
		loadInfo.setVehicles(vehicles);
		
		//LoadInfo Additional Numbers 
		List<LoadInfoAdditionalNumbers> lians = new ArrayList<LoadInfoAdditionalNumbers>();
		LoadInfoAdditionalNumbers l1 = new LoadInfoAdditionalNumbers();
		l1.setId("55f001c244ae1d5f45b17348");
		
		LoadInfoAdditionalNumbers l2 = new LoadInfoAdditionalNumbers();
		l2.setId("55f2819844ae1dc44684a110");
		
		lians.add(l1);
		lians.add(l2);
		
		loadInfo.setLoadinfoAddtionalNumbers(lians);
		
		// BillofLanding 
		List<BillofLanding> bols = new ArrayList<BillofLanding>();
		bols.add(new BillofLanding("55d456c344aee4a429274858"));
		bols.add(new BillofLanding("55d456cd44aee4a42927485a"));
		
		loadInfo.setBillofLandings(bols);
		
		return loadInfo;
	}
	
	@Override
	@Test
	public void createTest() {
		LoadInfo loadInfo = createNewLoadInfo();
		LoadInfo loadInfoSaved = loadinfoService.create(loadInfo);
		
		assertNotNull(loadInfoSaved);
		assertNotNull(loadInfoSaved.getId());
		
	}

	@Override
	@Test
	public void updateTest() {
		LoadInfo loadInfoSaved = loadinfoService.create(createNewLoadInfo());
		LoadInfo loadInfo = createNewLoadInfo();
		
		loadInfo.setUsername("Test User");
		loadInfo.setDirverMiles(50);
		loadInfo.setDriverHourly(60);
		loadInfo.setNoOfUnits(50);
		
		loadinfoService.update(loadInfoSaved, loadInfo);
		assertNotNull(loadInfo);
		
	}

	@Override
	@Test
	public void findTest() {
		LoadInfo loadInfoSaved = loadinfoService.create(createNewLoadInfo());
		LoadInfo loadInfo = loadinfoService.find(loadInfoSaved.getId());
		
		assertNotNull(loadInfo);

	}

	@Override
	@Test
	public void listTest() {
		this.createTest();
		List<LoadInfo> list = loadinfoService.list();
		
		assertNotNull(list);
		assertTrue(list.size() > 0);
		
	}

	@Override
	@Test
	public void deleteTest() {
		LoadInfo loadInfoSaved = loadinfoService.create(createNewLoadInfo());
		loadinfoService.delete(loadInfoSaved);
		
		LoadInfo loadInfo = loadinfoService.find(loadInfoSaved.getId());
		assertNull(loadInfo);
	}
	
	@Test
	public void searchTest(){
		LoadInfo loadInfoSaved = loadinfoService.create(createNewLoadInfo());
		
		List<LoadInfo> searchByStatus = loadinfoService.searchByStatus(loadInfoSaved.getStatus());
		List<LoadInfo> searchByType = loadinfoService.searchByType(loadInfoSaved.getType());
		List<LoadInfo> searchById = loadinfoService.searchById(loadInfoSaved.getId());
		
		assertNotNull(searchByStatus);
		assertTrue(searchByStatus.size() > 0);
		
		assertNotNull(searchByType);
		assertTrue(searchByType.size() > 0);
		
		assertNotNull(searchById);
		assertTrue(searchById.size() > 0);
	}

}
