package com.dev.logistic.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

import com.dev.logistic.validation.component.Phone;
 
/**
 * Use Phone validation constraint and implement 
 * generic mechanism to validate phone number fields.
 * 
 * <p>
 * This validator allows only 1234567890, 123-456-7890, (123)-456-7890 phone number format. 
 * Anything else will be prevented to access.
 * 
 * <p>
 * This validate phone field value and if field contains
 * invalid value then return false to show validation message
 * 
 * @author vasim
 *
 */
public class PhoneValidator implements ConstraintValidator<Phone, String>{

	@Override
	public void initialize(Phone phone) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean isValid(String phone, ConstraintValidatorContext arg1) {
		 //Validate phone numbers of format "1234567890"
		
		if(StringUtils.isNotBlank(phone)){
			
			if (phone.matches("\\d{10}")) return true;

		       //validating phone number with -, . or spaces
		       else if(phone.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) return true;
		        
		        //validating phone number where area code is in braces ()
		       else if(phone.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}")) return true;

		        //return false if nothing matches the input
		       else return false;
		}
       		
		return true;
	}
}
