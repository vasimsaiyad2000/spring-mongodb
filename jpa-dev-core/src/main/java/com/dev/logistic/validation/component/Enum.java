package com.dev.logistic.validation.component;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import com.dev.logistic.validation.EnumValidator;

/**
 * Enum validation constraint is used to validate enum field value.This constraint can be
 * applied using @Enum annotation on any enum field in model object. Custom message can be 
 * passed to constraint using @Enum(message = "Your message")
 * 
 *  <p>
 *  To use this generic enum validation contrainst you need to pass class of enum like 
 *  @Enum(enumClass = LoadStatus.class) which allow validated by class to perform validation for 
 *  fields in LoadStatus enum.
 * 
 * @author vasim
 *
 */

@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EnumValidator.class)
@Documented
public @interface Enum {

	String message() default "{Enum}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    Class<? extends java.lang.Enum<?>> enumClass();
    boolean ignoreCase() default false;
}
