package com.dev.logistic.validation.component;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import com.dev.logistic.validation.UniqueValidator;


/**
 * Unique validation constraint is used to validate unique value.This constraint can be
 * applied using @Unique annotation on any field whose value must be unique in model object. 
 * Custom message can be passed to constraint using @Unique(message = "Your message")
 * 
 * <p>
 * <p>
 *  To use this generic unique validation contrainst you need to pass service class with field name whose value 
 *  must be unique like @Unique(service = UserService.class, fieldName = "userName", message = "message") 
 *  which allow validated by class to perform unique validation for fields in UserService class.
 * 
 * @author vasim
 *
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UniqueValidator.class)
@Documented
public @interface Unique {
	
	 String message() default "{Unique}";
	 Class<?>[] groups() default {};
	 Class<? extends Payload>[] payload() default {};
	 Class<? extends FieldValueExists> service();
	 String serviceQualifier() default "";
	 String fieldName();
}
