package com.dev.logistic.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

import com.dev.logistic.validation.component.Number;

/**
 * Use Number validation constraint and implement 
 * generic mechanism to validate numeric value
 * 
 * <p>
 * This validate number value and if field contains
 * invalid value then return false to show validation message
 * 
 * @author vasim
 *
 */
public class NumberValidator implements ConstraintValidator<Number, String>{

	@Override
	public void initialize(Number arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isValid(String number, ConstraintValidatorContext arg1) {

		if(StringUtils.isNotBlank(number)){
			boolean isNumeric = StringUtils.isNumeric(number);
			if(!isNumeric) return false;
		}
		
		return true;
	}
}
