package com.dev.logistic.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.dev.logistic.validation.component.Enum;

/**
 * Use Enum validation constraint and implement 
 * generic mechanism to validate enum value.
 * 
 * <p>
 * This validate any enum field value and if field contains
 * invalid value then return false to show validation message
 * 
 * @author vasim
 *
 */
public class EnumValidator implements ConstraintValidator<Enum, String>{

	private Enum annotation;
	
	@Override
	public void initialize(Enum annotation) {
		this.annotation = annotation;
	}
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext arg1) {
		boolean result = false;
        
        Object[] enumValues = this.annotation.enumClass().getEnumConstants();
        
        if(enumValues != null)
        {
            for(Object enumValue:enumValues)
            {
                if(value.equals(enumValue.toString())){
                    result = true; 
                    break;
                }
            }
        }
        
		return result;
	}
}
