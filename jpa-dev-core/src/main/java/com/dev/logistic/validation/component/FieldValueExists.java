package com.dev.logistic.validation.component;

/**
 * Interface to defined generic method for unique field value check.
 * @author vasim
 *
 */
public interface FieldValueExists {

	public boolean fieldValueExists(Object value, String fieldName) throws UnsupportedOperationException;
}
