package com.dev.logistic.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.dev.logistic.command.LoadInfoCommand;

/**
 * Class helps to validation fields for LoadInfo entity.
 * It holds mechanism to validate required fields in LoadInfo entity.
 * <p>
 * 
 * This class implements org.springframework.validation.Validator class and 
 * override it's validate method with custom validation logic.
 *   
 * @author vasim
 *
 */
@Component
public class LoadInfoValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return LoadInfoCommand.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object command, Errors errors) {
		LoadInfoCommand loadInfo = (LoadInfoCommand) command;
		
		if(loadInfo.getDriver() == null){
			errors.rejectValue("driver", "NotNull", "loadinfo.driver.required");
		}
		
		if(loadInfo.getCarrier() == null){
			errors.rejectValue("carrier", "NotNull", "loadinfo.carrier.required");
		}
		
		if(loadInfo.getInvoicee() == null){
			errors.rejectValue("invoicee", "NotNull", "loadinfo.invoicee.required");
		}
		
		if(loadInfo.getShippers().size() == 0){
			errors.rejectValue("shippers", "NotNull", "loadinfo.shipper.required");
		}
		
		if(loadInfo.getConsignees().size() == 0){
			errors.rejectValue("consignees", "NotNull", "loadinfo.consignee.required");
		}
		
		if(loadInfo.getVehicles().size() == 0){
			errors.rejectValue("vehicles", "NotNull", "loadinfo.vehicle.required");
		}
		
		if(loadInfo.getBillofLandings().size() == 0){
			errors.rejectValue("billofLandings", "NotNull", "loadinfo.bol.required");
		}
		
		if(loadInfo.getLoadinfoAddtionalNumbers().size() == 0){
			errors.rejectValue("loadinfoAddtionalNumbers", "NotNull", "loadinfo.lian.required");
		}
	}

}
