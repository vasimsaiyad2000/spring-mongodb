package com.dev.logistic.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.dev.logistic.validation.component.FieldValueExists;
import com.dev.logistic.validation.component.Unique;

/**
 * Use Unique validation constraint and implement 
 * generic mechanism to validate unique field value.
 * 
 * <p>
 * This validate unique field value and if field contains
 * invalid value then return false to show validation message
 * 
 * @author vasim
 *
 */
public class UniqueValidator implements ConstraintValidator<Unique, Object>{

	@Autowired
	private ApplicationContext applicationContext;
	private FieldValueExists service;
    private String fieldName;
    
	@Override
	public void initialize(Unique unique) {
		Class<? extends FieldValueExists> clazz = unique.service();
        this.fieldName = unique.fieldName();
        String serviceQualifier = unique.serviceQualifier();

        if (StringUtils.isNotBlank(serviceQualifier)) {
            this.service = this.applicationContext .getBean(serviceQualifier, clazz);
        } else {
            this.service = this.applicationContext.getBean(clazz);
        }
	}

	@Override
	public boolean isValid(Object o, ConstraintValidatorContext ct) {
		return !this.service.fieldValueExists(o, this.fieldName);
	}

}
