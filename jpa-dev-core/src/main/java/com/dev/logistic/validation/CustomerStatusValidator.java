package com.dev.logistic.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.dev.logistic.validation.component.CustomerStatus;
/**
 * Use CustomStatus validation constraint and implement 
 * validation for CustomerStatus enum fields.
 * 
 * <p>
 * This validate CustomStatus enum field value and if field contains
 * invalid value then return false to show validation message
 * 
 * @author vasim
 *
 */
public class CustomerStatusValidator implements ConstraintValidator<CustomerStatus, String>{

	@Override
	public void initialize(CustomerStatus constraintAnnotation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		com.dev.logistic.constants.CustomerStatus[] statuses = com.dev.logistic.constants.CustomerStatus.values();
		boolean result = false;

		if(statuses != null){
		
			for(com.dev.logistic.constants.CustomerStatus status : statuses){
				if(status.value().equals(value)){
					result = true;
					break;
				}
			}	
		}
		
		return result;
	}

}
