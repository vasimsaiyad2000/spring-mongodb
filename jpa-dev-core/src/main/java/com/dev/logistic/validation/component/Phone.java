package com.dev.logistic.validation.component;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.dev.logistic.validation.PhoneValidator;

/**
 * Phone validation constraint is used to validate phone numbers.This constraint can be
 * applied using @Phone annotation on any field whose value must be valid phone number in model object. 
 * Custom message can be passed to constraint using @Phone(message = "Your message")
 * 
 * @author vasim
 *
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PhoneValidator.class)
@Documented
public @interface Phone {
	
	String message() default "{Phone}";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
