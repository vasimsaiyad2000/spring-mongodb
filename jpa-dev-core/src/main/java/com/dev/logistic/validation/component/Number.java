package com.dev.logistic.validation.component;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.dev.logistic.validation.NumberValidator;
import com.dev.logistic.validation.PhoneValidator;

/**
 * Number validation constraint is used to validate numeric value.This constraint can be
 * applied using @Number annotation on any field in model object. Custom message can be 
 * passed to constraint using @Number(message = "Your message")
 * 
 * @author vasim
 *
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NumberValidator.class)
@Documented
public @interface Number {

	String message() default "{Number}";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
