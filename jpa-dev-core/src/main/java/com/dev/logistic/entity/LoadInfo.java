package com.dev.logistic.entity;

import java.util.List;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import com.dev.logistic.constants.LoadStatus;

@Document(collection = "loadinfo")
public class LoadInfo extends BaseEntity {

	private String username;
	private String salesRepid;
	private String dispatcherId;
	private LoadStatus status;
	private int workOrder;
	private int type; 
	private float rate;
	private float noOfUnits;
	private float p_by_d;
	private float f_s_c;
	private float otherCharges;
	private float totalRate;
	private String equipmentId;
	private float dirverFlatRate;
	private float proratedMiles;
	private float proratedEmptyMiles;
	private float dirverMiles;
	private float dirverEmptyMiles;
	private float driverHourly;
	
	@DBRef
	private Driver driver;
	@DBRef
	private Carrier carrier;
	@DBRef
	private Invoicee invoicee;
	@DBRef
	private List<Shipper> shippers;
	@DBRef
	private List<Consignee> consignees;
	@DBRef
	private List<Vehicle> vehicles;
	@DBRef
	private List<LoadInfoAdditionalNumbers> loadinfoAddtionalNumbers;
	@DBRef
	private List<BillofLanding> billofLandings;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getSalesRepid() {
		return salesRepid;
	}
	public void setSalesRepid(String salesRepid) {
		this.salesRepid = salesRepid;
	}
	public String getDispatcherId() {
		return dispatcherId;
	}
	public void setDispatcherId(String dispatcherId) {
		this.dispatcherId = dispatcherId;
	}
	public LoadStatus getStatus() {
		return status;
	}
	public void setStatus(LoadStatus status) {
		this.status = status;
	}
	public int getWorkOrder() {
		return workOrder;
	}
	public void setWorkOrder(int workOrder) {
		this.workOrder = workOrder;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public float getRate() {
		return rate;
	}
	public void setRate(float rate) {
		this.rate = rate;
	}
	public float getNoOfUnits() {
		return noOfUnits;
	}
	public void setNoOfUnits(float noOfUnits) {
		this.noOfUnits = noOfUnits;
	}
	public float getP_by_d() {
		return p_by_d;
	}
	public void setP_by_d(float p_by_d) {
		this.p_by_d = p_by_d;
	}
	public float getF_s_c() {
		return f_s_c;
	}
	public void setF_s_c(float f_s_c) {
		this.f_s_c = f_s_c;
	}
	public float getOtherCharges() {
		return otherCharges;
	}
	public void setOtherCharges(float otherCharges) {
		this.otherCharges = otherCharges;
	}
	public float getTotalRate() {
		return totalRate;
	}
	public void setTotalRate(float totalRate) {
		this.totalRate = totalRate;
	}
	public String getEquipmentId() {
		return equipmentId;
	}
	public void setEquipmentId(String equipmentId) {
		this.equipmentId = equipmentId;
	}
	public float getDirverFlatRate() {
		return dirverFlatRate;
	}
	public void setDirverFlatRate(float dirverFlatRate) {
		this.dirverFlatRate = dirverFlatRate;
	}
	public float getProratedMiles() {
		return proratedMiles;
	}
	public void setProratedMiles(float proratedMiles) {
		this.proratedMiles = proratedMiles;
	}
	public float getProratedEmptyMiles() {
		return proratedEmptyMiles;
	}
	public void setProratedEmptyMiles(float proratedEmptyMiles) {
		this.proratedEmptyMiles = proratedEmptyMiles;
	}
	public float getDirverMiles() {
		return dirverMiles;
	}
	public void setDirverMiles(float dirverMiles) {
		this.dirverMiles = dirverMiles;
	}
	public float getDirverEmptyMiles() {
		return dirverEmptyMiles;
	}
	public void setDirverEmptyMiles(float dirverEmptyMiles) {
		this.dirverEmptyMiles = dirverEmptyMiles;
	}
	public float getDriverHourly() {
		return driverHourly;
	}
	public void setDriverHourly(float driverHourly) {
		this.driverHourly = driverHourly;
	}
	public Driver getDriver() {
		return driver;
	}
	public void setDriver(Driver driver) {
		this.driver = driver;
	}
	public Carrier getCarrier() {
		return carrier;
	}
	public void setCarrier(Carrier carrier) {
		this.carrier = carrier;
	}
	public Invoicee getInvoicee() {
		return invoicee;
	}
	public void setInvoicee(Invoicee invoicee) {
		this.invoicee = invoicee;
	}
	public List<Shipper> getShippers() {
		return shippers;
	}
	public void setShippers(List<Shipper> shippers) {
		this.shippers = shippers;
	}
	public List<Consignee> getConsignees() {
		return consignees;
	}
	public void setConsignees(List<Consignee> consignees) {
		this.consignees = consignees;
	}
	public List<Vehicle> getVehicles() {
		return vehicles;
	}
	public void setVehicles(List<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}
	public List<LoadInfoAdditionalNumbers> getLoadinfoAddtionalNumbers() {
		return loadinfoAddtionalNumbers;
	}
	public void setLoadinfoAddtionalNumbers(
			List<LoadInfoAdditionalNumbers> loadinfoAddtionalNumbers) {
		this.loadinfoAddtionalNumbers = loadinfoAddtionalNumbers;
	}
	public List<BillofLanding> getBillofLandings() {
		return billofLandings;
	}
	public void setBillofLandings(List<BillofLanding> billofLandings) {
		this.billofLandings = billofLandings;
	}
}