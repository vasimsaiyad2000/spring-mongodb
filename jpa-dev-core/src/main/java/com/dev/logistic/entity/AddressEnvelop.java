package com.dev.logistic.entity;


import org.hibernate.validator.constraints.Email;
import org.springframework.data.mongodb.core.mapping.Document;

import com.dev.logistic.validation.component.CustomerStatus;
import com.dev.logistic.validation.component.Enum;
import com.dev.logistic.validation.component.Phone;

@Document(collection = "addressEnvelop")
public class AddressEnvelop extends BaseEntity{
	
	private String primaryContact;
	@Phone(message = "address.phone1.invalid")
	private String telephone1;
	
	@Phone(message = "address.phone2.invalid")
	private String telephone2;
	private String ext1;
	private String ext2;
	private String tollFree;
	private String fax;
	private String secondaryContact;
	@Email(message = "address.email.invalid")
	private String email;
	@Email(message = "address.billing.email.invalid")
	private String billingEmail;
	private String mcLinkNumber;
	private Address address;
	private Address billingAddress;
	
	
	@CustomerStatus(message = "address.status.invalid")
	private String status;

	public String getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}

	public String getTelephone1() {
		return telephone1;
	}

	public void setTelephone1(String telephone1) {
		this.telephone1 = telephone1;
	}

	public String getTelephone2() {
		return telephone2;
	}

	public void setTelephone2(String telephone2) {
		this.telephone2 = telephone2;
	}

	public String getExt1() {
		return ext1;
	}

	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}

	public String getExt2() {
		return ext2;
	}

	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}

	public String getTollFree() {
		return tollFree;
	}

	public void setTollFree(String tollFree) {
		this.tollFree = tollFree;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getSecondaryContact() {
		return secondaryContact;
	}

	public void setSecondaryContact(String secondaryContact) {
		this.secondaryContact = secondaryContact;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBillingEmail() {
		return billingEmail;
	}

	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}

	public String getMcLinkNumber() {
		return mcLinkNumber;
	}

	public void setMcLinkNumber(String mcLinkNumber) {
		this.mcLinkNumber = mcLinkNumber;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Address getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
