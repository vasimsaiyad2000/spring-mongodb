package com.dev.logistic.entity;

import javax.validation.Valid;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "billoflanding")
public class BillofLanding extends BaseEntity {
	
	private String bol;
	private String thirdparty;
	private String driver;
	private String origin;
	private String destination;
	private String emgergencynum;
	private String cod;
	private String value;
	private String notes;
	private String itemPieces;
	private String itemDesc;
	private String itemLbs;
	private String itemType; 
	private String itemNMFC;
	private String itemClass;
	private String itemHM;
	
	@DBRef @Valid
	private AddressEnvelop addressEnvelop;

	public BillofLanding(){}
	
	public BillofLanding(String id){
		super(id);
	}
	
	public String getBol() {
		return bol;
	}

	public void setBol(String bol) {
		this.bol = bol;
	}

	public String getThirdparty() {
		return thirdparty;
	}

	public void setThirdparty(String thirdparty) {
		this.thirdparty = thirdparty;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getEmgergencynum() {
		return emgergencynum;
	}

	public void setEmgergencynum(String emgergencynum) {
		this.emgergencynum = emgergencynum;
	}

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getItemPieces() {
		return itemPieces;
	}

	public void setItemPieces(String itemPieces) {
		this.itemPieces = itemPieces;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getItemLbs() {
		return itemLbs;
	}

	public void setItemLbs(String itemLbs) {
		this.itemLbs = itemLbs;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getItemNMFC() {
		return itemNMFC;
	}

	public void setItemNMFC(String itemNMFC) {
		this.itemNMFC = itemNMFC;
	}

	public String getItemClass() {
		return itemClass;
	}

	public void setItemClass(String itemClass) {
		this.itemClass = itemClass;
	}

	public String getItemHM() {
		return itemHM;
	}

	public void setItemHM(String itemHM) {
		this.itemHM = itemHM;
	}

	public AddressEnvelop getAddressEnvelop() {
		return addressEnvelop;
	}

	public void setAddressEnvelop(AddressEnvelop addressEnvelop) {
		this.addressEnvelop = addressEnvelop;
	}
}
