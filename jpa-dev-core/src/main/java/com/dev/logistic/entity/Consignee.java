package com.dev.logistic.entity;

import javax.validation.Valid;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "consignee")
public class Consignee extends BaseEntity {

	@DBRef @Valid
	private AddressEnvelop addressEnvelop;

	public Consignee(){}
	
	public Consignee(String id){
		super(id);
	}
	
	public AddressEnvelop getAddressEnvelop() {
		return addressEnvelop;
	}

	public void setAddressEnvelop(AddressEnvelop addressEnvelop) {
		this.addressEnvelop = addressEnvelop;
	}

}
