package com.dev.logistic.entity;

import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.qmino.miredot.annotations.MireDotIgnore;

/**
 * 
 * @author vasim
 *
 */
public class BaseEntity {

	@MireDotIgnore
	@Id
	private String id;
		
	@MireDotIgnore
	@Version
	private Long version;
	
	@MireDotIgnore
	@DateTimeFormat(iso = ISO.DATE_TIME)
	@CreatedDate
	private DateTime createdDate;
	
	@MireDotIgnore
	@DateTimeFormat(iso = ISO.DATE_TIME)
	@LastModifiedDate
	private DateTime lastModifiedDate;
	
	public BaseEntity(){}
	
	public BaseEntity(String id){
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public DateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(DateTime createdDate) {
		this.createdDate = createdDate;
	}

	public DateTime getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(DateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

}
