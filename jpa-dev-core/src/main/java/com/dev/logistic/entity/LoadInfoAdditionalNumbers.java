package com.dev.logistic.entity;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "loadinfoadditionalnumbers")
public class LoadInfoAdditionalNumbers extends BaseEntity {

	@NotBlank(message = "lian.key.required")
	private String key;
	
	@NotBlank(message = "lian.value.required")
	private String value;
	private Byte showOnBits = 0;
	
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Byte getShowOnBits() {
		return showOnBits;
	}

	public void setShowOnBits(Byte showOnBits) {
		this.showOnBits = showOnBits;
	}
}
