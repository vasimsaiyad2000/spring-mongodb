package com.dev.logistic.entity;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * This is a model class that persist with mongodb vehicle collection and contains vehicle fields.
 * @author vasim
 *
 */
@Document(collection = "vehicle")
public class Vehicle extends BaseEntity {

	@NotBlank(message = "vehicle.type.required")
	private String type;
	
	public Vehicle(){}
	
	public Vehicle(String id){
		super(id);
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
