package com.dev.logistic.entity;

import javax.validation.Valid;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "shipper")
public class Shipper extends BaseEntity {

	private String shippingNotes;
	private String internalNotes;
	
	@DBRef @Valid
	private AddressEnvelop addressEnvelop;

	public Shipper(){}
	
	public Shipper(String id){
		super(id);
	}
	
	public String getShippingNotes() {
		return shippingNotes;
	}

	public void setShippingNotes(String shippingNotes) {
		this.shippingNotes = shippingNotes;
	}

	public String getInternalNotes() {
		return internalNotes;
	}

	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}

	public AddressEnvelop getAddressEnvelop() {
		return addressEnvelop;
	}

	public void setAddressEnvelop(AddressEnvelop addressEnvelop) {
		this.addressEnvelop = addressEnvelop;
	}
	
}
