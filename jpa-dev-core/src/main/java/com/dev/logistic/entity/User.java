package com.dev.logistic.entity;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.mongodb.core.mapping.Document;

import com.qmino.miredot.annotations.MireDotIgnore;


@Document(collection = "user")
@JsonSerialize(include=Inclusion.ALWAYS)
public class User extends BaseEntity{
	
	@NotBlank(message = "user.firstname.required")
	private String firstName;
	private String lastName;
	
	@NotBlank(message = "user.username.required")
	private String userName;
	
	@NotBlank(message = "user.email.required")
	@Email(message = "user.email.invalid")
	private String email;
	
	@NotBlank(message = "user.password.required")
	private String password;

	@MireDotIgnore
	private List<String> roles;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<String> getRoles() {
		return roles;
	}
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
	
}
