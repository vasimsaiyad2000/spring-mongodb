package com.dev.logistic.entity;

import javax.validation.Valid;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "carrier")
public class Carrier extends BaseEntity{
	
	private String receivingNotes;
	private String internalNotes;
	
	@DBRef @Valid
	private AddressEnvelop addressEnvelop;

	public Carrier(){}
	
	public Carrier(String id){
		super(id);
	}
	
	public String getReceivingNotes() {
		return receivingNotes;
	}

	public void setReceivingNotes(String receivingNotes) {
		this.receivingNotes = receivingNotes;
	}

	public String getInternalNotes() {
		return internalNotes;
	}

	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}

	public AddressEnvelop getAddressEnvelop() {
		return addressEnvelop;
	}

	public void setAddressEnvelop(AddressEnvelop addressEnvelop) {
		this.addressEnvelop = addressEnvelop;
	}

}
