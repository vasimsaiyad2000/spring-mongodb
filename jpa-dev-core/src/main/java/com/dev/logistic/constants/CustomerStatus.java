package com.dev.logistic.constants;

/**
 * This handle customer status. Below is the list of valid values.
 * Prospect Active, Prospect Inactive, Active, Inactive
 * 
 * @author vasim
 *
 */
public enum CustomerStatus {

	
	PROSPECT_ACTIVE {
		@Override
		public String value() {
			// TODO Auto-generated method stub
			return "Prospect Active";
		}
	}, 
	PROSPECT_INACTIVE {
		@Override
		public String value() {
			// TODO Auto-generated method stub
			return "Prospect Inactive";
		}
	}, 
	ACTIVE {
		@Override
		public String value() {
			// TODO Auto-generated method stub
			return "Active";
		}
	}, 
	INACTIVE {
		@Override
		public String value() {
			// TODO Auto-generated method stub
			return "Inactive";
		}
	};
	
	public abstract String value();
}
