package com.dev.logistic.constants;

/**
 * This handle load status. Below is the list of valid values. 
 * Open, Covered, Dispatched, Loading, OnRoute, Unloading, OnYard, Delivered, Completed, Refused 
 * @author vasim
 *
 */
public enum LoadStatus {
	Open, Covered, Dispatched, Loading, OnRoute, Unloading, OnYard, Delivered, Completed, Refused;
	
	public static LoadStatus valueOfStatus(String value){
	    switch (value.toUpperCase()) {
			case "OPEN":
				return Open;
			case "COVERED":
				return Covered;
			case "DISPATCHED":
				return Dispatched;
			case "LOADING":
				return Loading;
			case "ONROUTE":
				return OnRoute;
			case "UNLOADING":
				return Unloading;
			case "ONYARD":
				return OnYard;
			case "DELIVERED":
				return Delivered;
			case "COMPLETED":
				return Completed;
			case "REFUSED":
				return Refused;
			default:
				return null;
		}
	}
}
