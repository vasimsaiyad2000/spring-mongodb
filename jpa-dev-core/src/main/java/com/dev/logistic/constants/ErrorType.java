package com.dev.logistic.constants;

/**
 * This handle error type. Below is the list of valid values.
 * INTERNAL_SERVER_ERROR, BAD_REQUEST, NOT_FOUND, UNAUTHORIZED
 * @author vasim
 *
 */
public enum ErrorType {

	INTERNAL_SERVER_ERROR, BAD_REQUEST, NOT_FOUND, UNAUTHORIZED;
}
