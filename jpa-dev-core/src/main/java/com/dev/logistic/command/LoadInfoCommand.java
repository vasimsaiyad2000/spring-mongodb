package com.dev.logistic.command;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

import com.dev.logistic.constants.LoadStatus;
import com.dev.logistic.entity.BillofLanding;
import com.dev.logistic.entity.Carrier;
import com.dev.logistic.entity.Consignee;
import com.dev.logistic.entity.Driver;
import com.dev.logistic.entity.Invoicee;
import com.dev.logistic.entity.LoadInfoAdditionalNumbers;
import com.dev.logistic.entity.Shipper;
import com.dev.logistic.entity.Vehicle;
import com.dev.logistic.validation.component.Enum;
import com.dev.logistic.validation.component.Number;

public class LoadInfoCommand {

	private String id;
	private String username;
	private String salesRepid;
	private String dispatcherId;
	
	@Enum(message = "loadinfo.status.enum", enumClass = LoadStatus.class)
	private String status;
	@Number(message = "loadinfo.workorder.number")
	private String workOrder;
	@Number(message = "loadinfo.type.number")
	private String type; 
	@Number(message = "loadinfo.rate.number")
	private String rate;
	@Number(message = "loadinfo.noOfUnit.number")
	private String noOfUnits;
	@Number(message = "loadinfo.p_by_d.number")
	private String p_by_d;
	@Number(message = "loadinfo.f_s_c.number")
	private String f_s_c;
	@Number(message = "loadinfo.otherCharges.number")
	private String otherCharges;
	@Number(message = "loadinfo.totalRate.number")
	private String totalRate;
	private String equipmentId;
	@Number(message = "loadinfo.diverFlatRate.number")
	private String dirverFlatRate;
	@Number(message = "loadinfo.proratedMiles.number")
	private String proratedMiles;
	@Number(message = "loadinfo.proratedEmptyMiles.number")
	private String proratedEmptyMiles;
	@Number(message = "loadinfo.dirverMiles.number")
	private String dirverMiles;
	@Number(message = "loadinfo.dirverEmptyMiles.number")
	private String dirverEmptyMiles;
	@Number(message = "loadinfo.driverHourly.number")
	private String driverHourly;
	
	private Driver driver;
	private Carrier carrier;
	private Invoicee invoicee;
	private List<Shipper> shippers;
	private List<Consignee> consignees;
	private List<Vehicle> vehicles;
	private List<LoadInfoAdditionalNumbers> loadinfoAddtionalNumbers;
	private List<BillofLanding> billofLandings;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getSalesRepid() {
		return salesRepid;
	}
	public void setSalesRepid(String salesRepid) {
		this.salesRepid = salesRepid;
	}
	public String getDispatcherId() {
		return dispatcherId;
	}
	public void setDispatcherId(String dispatcherId) {
		this.dispatcherId = dispatcherId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getWorkOrder() {
		return workOrder;
	}
	public void setWorkOrder(String workOrder) {
		this.workOrder = workOrder;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getNoOfUnits() {
		return noOfUnits;
	}
	public void setNoOfUnits(String noOfUnits) {
		this.noOfUnits = noOfUnits;
	}
	public String getP_by_d() {
		return p_by_d;
	}
	public void setP_by_d(String p_by_d) {
		this.p_by_d = p_by_d;
	}
	public String getF_s_c() {
		return f_s_c;
	}
	public void setF_s_c(String f_s_c) {
		this.f_s_c = f_s_c;
	}
	public String getOtherCharges() {
		return otherCharges;
	}
	public void setOtherCharges(String otherCharges) {
		this.otherCharges = otherCharges;
	}
	public String getTotalRate() {
		return totalRate;
	}
	public void setTotalRate(String totalRate) {
		this.totalRate = totalRate;
	}
	public String getEquipmentId() {
		return equipmentId;
	}
	public void setEquipmentId(String equipmentId) {
		this.equipmentId = equipmentId;
	}
	public String getDirverFlatRate() {
		return dirverFlatRate;
	}
	public void setDirverFlatRate(String dirverFlatRate) {
		this.dirverFlatRate = dirverFlatRate;
	}
	public String getProratedMiles() {
		return proratedMiles;
	}
	public void setProratedMiles(String proratedMiles) {
		this.proratedMiles = proratedMiles;
	}
	public String getProratedEmptyMiles() {
		return proratedEmptyMiles;
	}
	public void setProratedEmptyMiles(String proratedEmptyMiles) {
		this.proratedEmptyMiles = proratedEmptyMiles;
	}
	public String getDirverMiles() {
		return dirverMiles;
	}
	public void setDirverMiles(String dirverMiles) {
		this.dirverMiles = dirverMiles;
	}
	public String getDirverEmptyMiles() {
		return dirverEmptyMiles;
	}
	public void setDirverEmptyMiles(String dirverEmptyMiles) {
		this.dirverEmptyMiles = dirverEmptyMiles;
	}
	public String getDriverHourly() {
		return driverHourly;
	}
	public void setDriverHourly(String driverHourly) {
		this.driverHourly = driverHourly;
	}
	public Driver getDriver() {
		return driver;
	}
	public void setDriver(Driver driver) {
		this.driver = driver;
	}
	public Carrier getCarrier() {
		return carrier;
	}
	public void setCarrier(Carrier carrier) {
		this.carrier = carrier;
	}
	public Invoicee getInvoicee() {
		return invoicee;
	}
	public void setInvoicee(Invoicee invoicee) {
		this.invoicee = invoicee;
	}
	public List<Shipper> getShippers() {
		return shippers;
	}
	public void setShippers(List<Shipper> shippers) {
		this.shippers = shippers;
	}
	public List<Consignee> getConsignees() {
		return consignees;
	}
	public void setConsignees(List<Consignee> consignees) {
		this.consignees = consignees;
	}
	public List<Vehicle> getVehicles() {
		return vehicles;
	}
	public void setVehicles(List<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}
	public List<LoadInfoAdditionalNumbers> getLoadinfoAddtionalNumbers() {
		return loadinfoAddtionalNumbers;
	}
	public void setLoadinfoAddtionalNumbers(
			List<LoadInfoAdditionalNumbers> loadinfoAddtionalNumbers) {
		this.loadinfoAddtionalNumbers = loadinfoAddtionalNumbers;
	}
	public List<BillofLanding> getBillofLandings() {
		return billofLandings;
	}
	public void setBillofLandings(List<BillofLanding> billofLandings) {
		this.billofLandings = billofLandings;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

}
