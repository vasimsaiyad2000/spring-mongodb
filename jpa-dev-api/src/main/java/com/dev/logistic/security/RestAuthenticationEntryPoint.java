package com.dev.logistic.security;

import java.io.IOException;
import java.security.Principal;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import com.dev.logistic.constants.ErrorType;
import com.dev.logistic.exception.ErrorResource;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * This class generate unauthorized response for rest authentication service.
 * 
 * @author vasim
 *
 */
@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {

	private static Logger log = Logger.getLogger(RestAuthenticationEntryPoint.class);
	
	@Autowired
	private MessageSource messageSource;
	
	/**
	 * This method handle error response for unauthorized api access
	 */
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException ae) throws IOException, ServletException {
		
		log.info("Checking user is authorized to access rest api");
		
		ErrorResource error = new ErrorResource(ErrorType.UNAUTHORIZED, messageSource.getMessage("user.not.authorized", null, null));
		response.setContentType("application/json");
	  	response.setCharacterEncoding("UTF-8");
	  	response.setStatus(HttpStatus.UNAUTHORIZED.value());
	  	
	  	log.info("User is not authorized to access rest api");
	  	
	  	ObjectMapper mapper = new ObjectMapper();
	  	response.getWriter().write(mapper.writeValueAsString(error));
	}
}
