package com.dev.logistic.security;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import com.dev.logistic.entity.User;
import com.dev.logistic.repository.UserRepository;

/**
 * This class is responsible to make rest api secure.
 * <P>
 * Each API request comes with header name <b>Authorization</b> and header value <b>Basic (Base64 encode of <b>username:password</b>)</b>
 * <p>
 * This class check header against database and provide api access to valid user. 
 * <p>
 * If user is not authorized to access api, then user will get unauthorized error.
 * 
 *  
 * @author vasim
 *
 */
@Component
public class RestAuthenticationService implements UserDetailsService{


	private static Logger log = Logger.getLogger(RestAuthenticationService.class);
	
	@Autowired
	private UserRepository userRepository;
	private UserDetails userDetails;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub

		log.info("Authenticating " + username + " to access rest api");
		
		try{
			
			boolean enabled = true;
	        boolean accountNonExpired = true;
	        boolean credentialsNonExpired = true;
	        boolean accountNonLocked = true;
	        
	        User user = userRepository.findByUserName(username);
	        
	        if(user == null){
	        	log.info("Authentication failed for " +username + " to access rest api");
	        }
	       
	        userDetails = new org.springframework.security.core.userdetails.User(
	        				user.getUserName(), 
	        				user.getPassword(),
	        				enabled,
	        				accountNonExpired,
	        				credentialsNonExpired,
	        				accountNonLocked,
	        				getAuthorities(user.getRoles()));
	        
		}catch(Exception e){
			e.printStackTrace();
		}

		log.info(username + " authenticated successfully to access rest api");
		return userDetails;
	}
	
	public List<GrantedAuthority> getAuthorities(List<String> roles) {
        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
        
        for(String role : roles){
        	authList.add(new SimpleGrantedAuthority(role));	
        }
        
        return authList;
    }
}
