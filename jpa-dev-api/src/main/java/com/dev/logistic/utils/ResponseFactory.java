package com.dev.logistic.utils;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Response factory class to generate response for api.
 * 
 * @author vasim
 *
 */
@Component
public class ResponseFactory {

	@Autowired
	private MessageSource messageResource;
	
	/**
	 * Returns success response with status code 200.
	 * @param content Response object
	 * @return OK Response
	 */
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity ok(Object content){
		return new ResponseEntity(content);
	}
	
	/**
	 * Returns success response and message with status code 200.
	 * @param content Response object
	 * @param key Message key
	 * @param params Message parameters
	 * @return OK Response
	 */
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity ok(Object content, String key, Object ... params){
		return new ResponseEntity(content, messageResource.getMessage(key, params, Locale.getDefault()));
	}
}
