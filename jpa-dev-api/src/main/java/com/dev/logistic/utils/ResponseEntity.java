package com.dev.logistic.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * This class help to create success response to client from api.
 * If api execute without any issue then the class will generate appropriate
 * response based on given data and message. 
 *   
 * @author vasim
 *
 */
@JsonInclude(Include.NON_EMPTY)
public class ResponseEntity {

	private String message;
	private Object response;
	
	/**
	 * Generate response for given response object
	 * @param response Response object
	 */
	public ResponseEntity(Object response){
		this.response = response;
	}
	
	/**
	 * Generate response for given response object and message 
	 * @param response Response object
	 * @param message Message
	 */
	public ResponseEntity(Object response, String message){
		this.response = response;
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}
	
	@Override
	public String toString(){
		return "Message :" + message + "  ||  " + "Response :" + response.toString();
	}
}
