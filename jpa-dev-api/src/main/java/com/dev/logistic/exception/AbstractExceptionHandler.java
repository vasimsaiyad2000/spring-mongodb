package com.dev.logistic.exception;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.dev.logistic.constants.ErrorType;
import com.dev.logistic.exception.BadRequestException;
import com.dev.logistic.exception.ObjectNotFoundException;
 
/**
 * This class handle all exceptions thrown by any controller method at the same place.
 * <p>
 * It allows to apply exception handling at one place instead of applying in every controller method.
 * <p>
 * It handle below exceptions and provide appropriate response for exception to client.
 * <p>
 * 1. BadRequestException
 * <p>
 * 2.ObjectNotFoundException
 * <p>  
 * 3. Exception
 * 
 * @author vasim
 *
 */
@ControllerAdvice
public class AbstractExceptionHandler {

	private static Logger log = Logger.getLogger(AbstractExceptionHandler.class);
	private MessageSource messageResource;

	@Autowired
	public AbstractExceptionHandler(MessageSource messageResource){
		this.messageResource = messageResource;
	}
	
	/**
	 * Returns list of multiple fields validation errors.
	 * @param bre BadRequestException object
	 * @return a list of multiple fields validation error with 
	 * resource, field, code and message.  
	 * @throws IOException
	 */
	@ExceptionHandler(BadRequestException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorResource handleBadRequestException(BadRequestException bre) throws IOException{
		List<FieldErrorResource> fieldErrorResources = new ArrayList<>();

        List<FieldError> fieldErrors = bre.getErrors().getFieldErrors();
        for (FieldError fieldError : fieldErrors) {
            FieldErrorResource fieldErrorResource = new FieldErrorResource();
            fieldErrorResource.setResource(fieldError.getObjectName());
            fieldErrorResource.setField(fieldError.getField());
            fieldErrorResource.setCode(fieldError.getCode());
            fieldErrorResource.setMessage(messageResource.getMessage(fieldError.getDefaultMessage(), null, null));
            fieldErrorResources.add(fieldErrorResource);
        }
		
		log.error("Bad Request Exception :" + getStackTraceAsString(bre));
		log.error("Please rectify below validation errors :"+ "\n" +fieldErrorResources.toString());
		return new ErrorResource(ErrorType.BAD_REQUEST, fieldErrorResources);
	}
	
	/**
	 * Returns error response for unavailable object.
	 * @param ex ObjectNotFound object
	 * @return Error message and error type
	 * @throws IOException
	 */
	@ExceptionHandler(ObjectNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorResource handleObjectNotFoundException(ObjectNotFoundException ex) throws IOException{
		ex = new ObjectNotFoundException(messageResource.getMessage(ex.getKey(), null, null), ex);
		log.error("Object not found Exception :" + getStackTraceAsString(ex));
		return new ErrorResource(ErrorType.NOT_FOUND, ex.getMessage());
	}
	
	/**
	 * Returns error response for exception.
	 * @param ex Exception object
	 * @return Exception message and error type
	 * @throws IOException
	 */
	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ErrorResource handleInternalServerException(Exception ex) throws IOException{
		log.error("Internal Server Exception" + ex);
		return new ErrorResource(ErrorType.INTERNAL_SERVER_ERROR, ex.getMessage());
	}
	
	/**
	 * Returns stack trace as string
	 * @param exception Exception object
	 * @return String value of StackTrace
	 */
	private String getStackTraceAsString(Exception exception) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		pw.print(" [ ");
		pw.print(exception.getClass().getName());
		pw.print(" ] ");
		pw.print(exception.getMessage());
	
		return sw.toString();
	}
}



