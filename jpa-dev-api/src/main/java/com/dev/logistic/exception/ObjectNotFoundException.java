package com.dev.logistic.exception;

/**
 * This class is responsible to throw ObjectNotFoundException.
 * <p>
 * It is used to throw runtime exception when object which has been getting is unavailable.
 * @author vasim
 *
 */
public class ObjectNotFoundException extends AppException{

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	private String key;
	
	/**
	 * Default constructor
	 */
	public ObjectNotFoundException(){
		
	}
	
	/**
	 * Parameterized constructor
	 * @param key Message key to load exception message from resource bundle
	 */ 

	public ObjectNotFoundException(String key){
		this.key = key;
	}
	
	/**
	 * Parameterized constructor
	 * @param message Exception message
	 * @param cause Throwable object
	 */
	public ObjectNotFoundException(String message, Throwable cause){
		super(message, cause);
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	
}
