package com.dev.logistic.exception;

/**
 * This class holds validation fields details.
 * @author vasim
 *
 */
public class FieldErrorResource {

	private String resource;
    private String field;
    private String code;
    private String message;
 
	public String getField() {
		return field;
	}


	public void setField(String field) {
		this.field = field;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getResource() {
		return resource;
	}


	public void setResource(String resource) {
		this.resource = resource;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	@Override
	public String toString(){
		return "Field Name :" + this.field + " || " + "Message : " + this.message + "\n";
	}
}
