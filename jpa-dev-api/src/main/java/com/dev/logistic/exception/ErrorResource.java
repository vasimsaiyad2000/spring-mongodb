package com.dev.logistic.exception;

import java.util.List;
import com.dev.logistic.constants.ErrorType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * This class represent the response for various errors like validations, object not found.
 * @author vasim
 *
 */
@JsonInclude(Include.NON_EMPTY)
public class ErrorResource {

	private String message;
	private ErrorType error;
	private List<FieldErrorResource> fieldErrors;
	
	public ErrorResource(ErrorType error, String message){
		this.error = error;
		this.message = message;
	}
	
	public ErrorResource(ErrorType error, List<FieldErrorResource> fieldErrors){
		this.error = error;
		this.fieldErrors = fieldErrors;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ErrorType getError() {
		return error;
	}
	public void setError(ErrorType error) {
		this.error = error;
	}
	public List<FieldErrorResource> getFieldErrors() {
		return fieldErrors;
	}
	public void setFieldErrors(List<FieldErrorResource> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}
}
