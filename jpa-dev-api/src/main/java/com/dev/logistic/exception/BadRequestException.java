package com.dev.logistic.exception;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.springframework.validation.Errors;

/**
 * This class is responsible to throw bad request exception.
 * <p> 
 * This class is mostly used to throw validation errors.
 * 
 * @author vasim
 *
 */
public class BadRequestException extends AppException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Errors errors;

	/**
	 * Default constructor
	 */
	public BadRequestException() {
	}

	/**
	 * Parameterized constructor
	 * @param message Exception message
	 * @param errors Errors
	 */
	public BadRequestException(String message, Errors errors) {
		super(message);
		this.errors = errors;
	}

	/**
	 * Parameterized constructor
	 * @param cause Throwable object
	 */
	public BadRequestException(Throwable cause) {
		super(cause);
	}

	/**
	 * Parameterized constructor
	 * @param message Exception message
	 * @param cause Throwable object
	 */
	public BadRequestException(String message, Throwable cause) {
		super(message, cause);
	}
	
	/**
	 * Returns fields errors
	 * @return Errors
	 */
	public Errors getErrors() { return errors; }
}
