package com.dev.logistic.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dev.logistic.entity.AddressEnvelop;
import com.dev.logistic.entity.Carrier;
import com.dev.logistic.entity.Shipper;
import com.dev.logistic.exception.BadRequestException;
import com.dev.logistic.exception.ObjectNotFoundException;
import com.dev.logistic.service.AddressEnvelopService;
import com.dev.logistic.service.ShipperService;
import com.dev.logistic.utils.ResponseEntity;
/**
 * This is a rest api class that provide shipper services to client.
 * Any client can access shipper api through end point url.
 * 
 * <p>
 * Class perform below operations.
 * 
 * <p> 
 * 1. Save shipper
 * <p>
 * 2. Find shipper
 * <p>
 * 3. Update shipper
 * <p>
 * 4. Delete shipper
 * <p>
 * 5. List shippers
 * 
 * @author vasim
 *
 */
@Controller
@RequestMapping(value = "/api/shipper")
public class ShipperController extends BaseController<Shipper> {

	private static Logger log = Logger.getLogger(ShipperController.class);
	
	@Autowired
	private ShipperService shipperService;
	
	@Autowired
	private AddressEnvelopService addressEnvelopService;
	
	/**
	 * Create new shipper
	 * @param _shipper The shipper that will be created
	 * @param result BindingResult that hold validation results
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException 
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity create(@RequestBody @Valid Shipper _shipper, BindingResult results) throws Exception{
		
		validate(results);
		
		// Saving shipper address
		log.info("Saving shipper address ...");
		AddressEnvelop addressEnvelop = addressEnvelopService.create(_shipper.getAddressEnvelop());
		log.info("Shipper address saved successfully");
		
		// Saving shipper
		log.info("Saving shipper details...");
		_shipper.setAddressEnvelop(addressEnvelop);
		Shipper shipper = shipperService.create(_shipper);
		
		log.info("Shipper details saved successfully");
		return ok(shipper, "shipper.save.success");
	}
	
	/**
	 * Get the shipper with given id
	 * @statuscode 404 Shipper not found for given id
	 * @param id The shipper id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException 
	 */	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity find(@PathVariable("id") String id) throws Exception {
		log.info("Getting shipper details for {} " + id);
		Shipper shipper = shipperService.find(id);
		
		if(shipper != null){
			log.info("Shipper details found for {} " + id);
			return ok(shipper);	
		}
		
		throw new ObjectNotFoundException("shipper.not.found");
	}
	
	/**
	 * Update existing shipper
	 * @statuscode 404 Shipper not found for given id
	 * @param id The id of the shipper that will be updated
	 * @param _shipper The shipper details that will be updated
	 * @param result BindingResult object that holds validation results.
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException
	 */
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity update(@PathVariable("id") String id, @RequestBody @Valid Shipper _shipper, BindingResult results) throws Exception{
		
		Shipper shipper = shipperService.find(id);
		
		if(shipper != null){
			validate(results);
			
			// Updating shipper address
			log.info("Updating shipper address for {}" + shipper.getAddressEnvelop().getId());
			AddressEnvelop address = addressEnvelopService.update(shipper.getAddressEnvelop(), _shipper.getAddressEnvelop());
			log.info("Shipper address updated successfully for {} " + shipper.getAddressEnvelop().getId());
			
			// Updating shipper
			log.info("Updating shipper details for {} " + id);
			shipper.setAddressEnvelop(address);
			shipper = shipperService.update(shipper, _shipper);
			
			log.info("Shipper details updated successfully");
			return ok(shipper, "shipper.update.success");
		}
		
		throw new ObjectNotFoundException("shipper.not.found");
	}
	
	/**
	 * Delete shipper with given id
	 * @statuscode 404 Shipper not found for given id
	 * @param id The shipper id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException
	 */
	
	@RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity delete(@PathVariable("id") String id) throws Exception{
		log.info("Deleting shipper for {} " +id);
		Shipper shipper = shipperService.find(id);

		if(shipper != null){
			
			// Deleting shipper address
			log.info("Deleting shipper address for {} " + shipper.getAddressEnvelop().getId());
			addressEnvelopService.delete(shipper.getAddressEnvelop());
			log.info("Shipper address deleted successfully");
			
			// Deleting shipper
			shipperService.delete(shipper);
			
			log.info("Shipper deleted successfully");
			return ok(shipper, "shipper.delete.success");
		}
		
		throw new ObjectNotFoundException("shipper.not.found");
	}
	
	/**
	 * Get list of shippers
	 * @param start First index of results. Default value is 1.
	 * @param end Last index of results. Default value is 10.
	 * @param sortBy Name of the field that perform the sorting. Default value is id field.
	 * @param sortType Type of sorting. The valid values are either asc or desc. Default value is asc.
	 * @return Object of ResponseEntity
	 * @throws Exception throws Exception
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity list(@RequestParam(value = "start", defaultValue = "1") int start,
											 @RequestParam(value = "end", defaultValue = "10") int end,
											 @RequestParam(value = "sortBy", defaultValue = "id") String sortBy,
											 @RequestParam(value = "sortType", defaultValue = "asc") String sortType) throws Exception{
		
		log.info("Getting shippers list...");
		Page<Shipper> shippers = shipperService.list(start, end, sortBy, sortType);
		
		log.info("Shippers found {} " + shippers.getContent().size());
		return ok(shippers);
	}
}
