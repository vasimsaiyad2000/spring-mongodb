package com.dev.logistic.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.aspectj.weaver.Dump.INode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dev.logistic.entity.AddressEnvelop;
import com.dev.logistic.entity.Invoicee;
import com.dev.logistic.exception.BadRequestException;
import com.dev.logistic.exception.ObjectNotFoundException;
import com.dev.logistic.service.AddressEnvelopService;
import com.dev.logistic.service.InvoiceeService;
import com.dev.logistic.utils.ResponseEntity;
/**
 * This is a rest api class that provide invoicee services to client.
 * Any client can access invoicee api through end point url.
 * 
 * <p>
 * Class perform below operations.
 * 
 * <p> 
 * 1. Save invoicee
 * <p>
 * 2. Find invoicee 
 * <p>
 * 3. Update invoicee
 * <p>
 * 4. Delete invoicee
 * <p>
 * 5. List invoicees
 * 
 * @author vasim
 *
 */
@Controller
@RequestMapping(value = "/api/invoicee")
public class InvoiceeController extends BaseController<Invoicee>{

	private static Logger log = Logger.getLogger(InvoiceeController.class);
	
	@Autowired
	private InvoiceeService invoiceeService;
	
	@Autowired
	private AddressEnvelopService addressEnvelopService;
	
	/**
	 * Create new invoicee
	 * @param _invoicee The invoicee that will be created
	 * @param result BindingResult that hold validation results
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException 
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity create(@RequestBody @Valid Invoicee _invoicee, BindingResult result) throws Exception{

		validate(result);

		// Saving invoicee address
		log.info("Saving invoicee address...");
		AddressEnvelop addressEnvelop = addressEnvelopService.create(_invoicee.getAddressEnvelop());
		log.info("Invoicee address saved successfully");
		
		// Saving invoicee
		log.info("Saving invoicee details...");
		_invoicee.setAddressEnvelop(addressEnvelop);
		Invoicee invoicee = invoiceeService.create(_invoicee);
		
		log.info("Invoice saved successfully");
		return ok(invoicee, "invoicee.save.success");
	}
	
	/**
	 * Get the invoicee with given id
	 * @statuscode 404 Invoicee not found for given id
	 * @param id The invoicee id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException 
	 */	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity find(@PathVariable("id") String id) throws Exception {
		log.info("Getting invoicee for {} " + id);
		Invoicee invoicee = invoiceeService.find(id);
		
		if(invoicee != null){
			log.info("Invoicee found for {}" + id);
			return ok(invoicee);	
		}
		
		throw new ObjectNotFoundException("invoicee.not.found");
	}
	
	/**
	 * Update existing invoicee
	 * @statuscode 404 Invoicee not found for given id
	 * @param id The id of the invoicee that will be updated
	 * @param _invoicee The invoicee details that will be updated
	 * @param result BindingResult object that holds validation results.
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException
	 */
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity update(@PathVariable("id") String id, @RequestBody @Valid Invoicee _invoicee, BindingResult result) throws Exception{
		
		Invoicee invoicee = invoiceeService.find(id);
		
		if(invoicee != null){
			validate(result);
			
			log.info("Updating invoicee for {}" + id);
			
			// Updating invoicee address
			log.info("Updating invoicee address for {}" + invoicee.getAddressEnvelop().getId());
			AddressEnvelop address = addressEnvelopService.update(invoicee.getAddressEnvelop(), _invoicee.getAddressEnvelop());
			log.info("Address updated successfully for {} " + invoicee.getAddressEnvelop().getId());
			
			invoicee.setAddressEnvelop(address);
			log.info("Invoicee updated for {}" + id);
			return ok(invoicee, "invoicee.update.success");
		}
		
		throw new ObjectNotFoundException("invoicee.not.found");
		
	}
	
	/**
	 * Delete invoicee with given id
	 * @statuscode 404 Invoicee not found for given id
	 * @param id The invoicee id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException
	 */

	@RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity delete(@PathVariable("id") String id) throws Exception{
		log.info("Deleting invoicee for {} " + id);
		Invoicee invoicee = invoiceeService.find(id);
		
		if(invoicee != null){
			// Deleting invoicee address
			log.info("Deleting invoicee address for {} " + invoicee.getAddressEnvelop().getId());
			addressEnvelopService.delete(invoicee.getAddressEnvelop());
			log.info("Invoicee address deleted");
			
			// Deleting invoicee
			invoiceeService.delete(invoicee);
			log.info("Invoicee deleted for {} " +id);
			return ok(invoicee, "invoicee.delete.success");
		}
		
		throw new ObjectNotFoundException("invoicee.not.found");
	}
	
	/**
	 * Get list of invoicees
	 * @param start First index of results. Default value is 1.
	 * @param end Last index of results. Default value is 10.
	 * @param sortBy Name of the field that perform the sorting. Default value is id field.
	 * @param sortType Type of sorting. The valid values are either asc or desc. Default value is asc.
	 * @return Object of ResponseEntity
	 * @throws Exception throws Exception
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity list(@RequestParam(value = "start", defaultValue = "1") int start,
											 @RequestParam(value = "end", defaultValue = "10") int end,
											 @RequestParam(value = "sortBy", defaultValue = "id") String sortBy,
											 @RequestParam(value = "sortType", defaultValue = "asc") String sortType) throws Exception{
		log.info("Getting invoicee list...");
		Page<Invoicee> invoicees = invoiceeService.list(start, end, sortBy, sortType);
		
		log.info("Invoicee found {} " + invoicees.getContent().size());
		return ok(invoicees);
	}

}
