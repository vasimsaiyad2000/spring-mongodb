package com.dev.logistic.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import com.dev.logistic.exception.BadRequestException;
import com.dev.logistic.utils.ResponseEntity;
import com.dev.logistic.utils.ResponseFactory;
import com.qmino.miredot.annotations.MireDotIgnore;

/**
 * Generic base controller class for rest api that defined generic api operations.
 * It contains create, update, find, delete, list operations definition and 
 * class that extends BaseController<T> class needs to implements those operations.
 * 
 * @author vasim
 *
 * @param <T>
 */
@MireDotIgnore
public abstract class BaseController<T> {

	private static Logger log = Logger.getLogger(BaseController.class);
	
	@Autowired
	private ResponseFactory responseFactory;
	
	/**
	 * Return success response.
	 * @param content Response data
	 * @return Object of ResponseEntity
	 */
	public ResponseEntity ok(Object content){
		return responseFactory.ok(content);
	}
	
	/**
	 * Return success response.
	 * @param content Response data
	 * @param key Message key
	 * @param params Message params
	 * @return Object of ResponseEntity
	 */
	public ResponseEntity ok(Object content, String key, Object ... params){
		return responseFactory.ok(content, key, params);
	}

	/**
	 * The method check fields validation and return the appropriate validation result to client
	 * @param result Hold validation results
	 * @throws BadRequestException throws BadRequestException if validation failed 
	 */
	public void validate(BindingResult result) throws BadRequestException{

		log.info("Validating entity fields...");
		
		if(result.hasErrors()){
			log.info("Validation failed for entity fields , FieldErrors :" +result.getFieldErrorCount());
			throw new BadRequestException("Validation Error", result);
		}
		
		log.info("Fields validated successfully");
	}
	
	
	/**
	 * This is api method that save given entity and return saved entity as response to client.
	 * Also perform various validations like required field, email, phone number, number, enum fields etc..   
	 * @param entity Entity
	 * @param result BindingResult that hold validation results
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException 
	 */
	public abstract ResponseEntity create(T entity, BindingResult result) throws Exception;
	
	/**
	 * This is api method that find the entity for given id.
	 * @param id Entity id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException 
	 */
	public abstract ResponseEntity find(String id) throws Exception;
	
	/**
	 * This is api method that update given entity and return updated entity as response to client.
	 * Also perform various validations like required field, email, phone number, number, enum fields etc..
	 * @param id Entity id
	 * @param entity Entity object
	 * @param result BindingResult object that holds validation results.
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException
	 */
	public abstract ResponseEntity update(String id, T entity, BindingResult result) throws Exception;
	
	/**
	 * This is api method that delete given entity.
	 * @param id Entity id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException
	 */
	public abstract ResponseEntity delete(String id) throws Exception;
	
	/**
	 * This is api method that return the list of entities.
	 * @param start Start index of results
	 * @param end End index of results
	 * @param sortBy Name of the field that perform the sorting
	 * @param sortType Type of sorting like ascending or descending
	 * @return Object of ResponseEntity
	 * @throws Exception throws Exception
	 */
	public abstract ResponseEntity list(int start, int end, String sortBy, String sortType) throws Exception;

}