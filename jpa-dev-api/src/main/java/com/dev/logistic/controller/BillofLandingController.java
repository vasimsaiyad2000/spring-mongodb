package com.dev.logistic.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;

import com.dev.logistic.entity.AddressEnvelop;
import com.dev.logistic.entity.BillofLanding;
import com.dev.logistic.entity.Shipper;
import com.dev.logistic.exception.BadRequestException;
import com.dev.logistic.exception.ObjectNotFoundException;
import com.dev.logistic.service.AddressEnvelopService;
import com.dev.logistic.service.BillofLandingService;
import com.dev.logistic.utils.ResponseEntity;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
/**
 * This is a rest api class that provide billoflanding services to client.
 * Any client can access billoflanding api through end point url.
 * 
 * <p>
 * Class perform below operations.
 * 
 * <p> 
 * 1. Save billoflanding
 * <p>
 * 2. Find billoflanding
 * <p>
 * 3. Update billoflanding
 * <p>
 * 4. Delete billoflanding
 * <p>
 * 5. List billoflanding
 * 
 * @author vasim
 *
 */
@Controller
@RequestMapping(value = "/api/billoflanding")
public class BillofLandingController extends BaseController<BillofLanding> {

	private static Logger log = Logger.getLogger(BillofLandingController.class);
	
	@Autowired
	private BillofLandingService billingofLandingService;
	
	@Autowired
	private AddressEnvelopService addressEnvelopService;
	
	/**
	 * Create new billoflanding
	 * @param _bol The billoflanding that will be created
	 * @param result BindingResult that hold validation results
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException 
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity create(@RequestBody @Valid BillofLanding _bol, BindingResult result) throws Exception{
		
		validate(result);
		
		// Saving BOL address
		log.info("Saving billoflanding address...");
		AddressEnvelop addressEnvelop = addressEnvelopService.create(_bol.getAddressEnvelop());
		log.info("Billoflanding address saved successfully");
		
		// Saving BOL
		log.info("Saving billoflanding details....");
		_bol.setAddressEnvelop(addressEnvelop);
		BillofLanding bol = billingofLandingService.create(_bol);
		
		log.info("Billoflanding saved successfully");
		return ok(bol, "bol.save.success");
	}
	
	/**
	 * Get the billoflanding with given id
	 * @statuscode 404 BillofLanding not found for given id
	 * @param id The billoflanding id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException 
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity find(@PathVariable("id") String id) throws Exception {
		log.info("Getting billoflanding for {} " +id);
		BillofLanding bol = billingofLandingService.find(id);
		
		if(bol != null){
			log.info("Billoflanding found for {}" + id);
			return ok(bol);	
		}
		
		throw new ObjectNotFoundException("bol.not.found");
	}
	
	/**
	 * Update existing billoflanding
	 * @statuscode 404 BillofLanding not found for given id
	 * @param id The id of the billoflanding that will be updated
	 * @param _bol The billoflanding details that will be updated
	 * @param result BindingResult object that holds validation results.
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException
	 */
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity update(@PathVariable("id") String id, @RequestBody @Valid BillofLanding _bol, BindingResult result) throws Exception{
		
		BillofLanding bol = billingofLandingService.find(id);
		
		if(bol != null){
		
			validate(result);

			// Updating BOL address
			AddressEnvelop address = addressEnvelopService.update(bol.getAddressEnvelop(), _bol.getAddressEnvelop());
			
			// Updating BOL
			log.info("Updating billoflanding for {} " +id);
			bol.setAddressEnvelop(address);
			bol = billingofLandingService.update(bol, _bol);
			
			log.info("Billoflanding updated for {} " + id);
			return ok(bol, "bol.update.success");
		}

		throw new ObjectNotFoundException("bol.not.found");
	}
	
	/**
	 * Delete billoflanding with given id
	 * @statuscode 404 BillofLanding not found for given id
	 * @param id The billoflanding id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException
	 */
	@RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity delete(@PathVariable("id") String id) throws Exception{
		log.info("Deleting billoflanding for {} " +id);
		BillofLanding bol = billingofLandingService.find(id);

		if(bol != null){
			// Deleting BOL address
			addressEnvelopService.delete(bol.getAddressEnvelop());
			
			// Deleting BOL
			billingofLandingService.delete(bol);
			log.info("Billoflanding deleted for {} " +id);
			return ok(bol, "bol.delete.success");
		}
		
		throw new ObjectNotFoundException("bol.not.found");
	}
	
	/**
	 * Get list of billoflandings
	 * @param start First index of results. Default value is 1.
	 * @param end Last index of results. Default value is 10.
	 * @param sortBy Name of the field that perform the sorting. Default value is id field.
	 * @param sortType Type of sorting. The valid values are either asc or desc. Default value is asc.
	 * @return Object of ResponseEntity
	 * @throws Exception throws Exception
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity list(@RequestParam(value = "start", defaultValue = "1") int start,
										     @RequestParam(value = "end", defaultValue = "10") int end,
										     @RequestParam(value = "sortBy", defaultValue = "id") String sortBy,
										     @RequestParam(value = "sortType", defaultValue = "asc") String sortType) throws Exception{
		
		log.info("Getting billoflandings list...");
		Page<BillofLanding> bols = billingofLandingService.list(start, end, sortBy, sortType);
		
		log.info("Billoflanding found {} " + bols.getContent().size());
		return ok(bols);
	}
}
