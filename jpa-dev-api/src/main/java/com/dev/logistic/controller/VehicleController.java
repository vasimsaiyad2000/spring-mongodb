package com.dev.logistic.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.dev.logistic.entity.Carrier;
import com.dev.logistic.entity.Vehicle;
import com.dev.logistic.exception.BadRequestException;
import com.dev.logistic.exception.ObjectNotFoundException;
import com.dev.logistic.service.VehicleService;
import com.dev.logistic.utils.ResponseEntity;

/**
 * This is a rest api class that provide vehicle services to client.
 * Any client can access vehicle api through end point url.
 * 
 * <p>
 * Class perform below operations.
 * 
 * <p> 
 * 1. Save vehicle
 * <p>
 * 2. Find vehicle 
 * <p>
 * 3. Update vehicle
 * <p>
 * 4. Delete vehicle 
 * <p>
 * 5. List vehicles
 * 
 * @author vasim
 *
 */

@Controller
@RequestMapping(value = "/api/vehicle")
public class VehicleController extends BaseController<Vehicle>{
		
	private static Logger log = Logger.getLogger(VehicleController.class);
	
	@Autowired
	private VehicleService vehicleService;

	/**
	 * Create new vehicle
	 * @param _vehicle The vehicle that will be created
	 * @param result BindingResult that hold validation results
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException 
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	public @ResponseBody ResponseEntity create(@RequestBody @Valid Vehicle _vehicle, BindingResult result) throws Exception{

		validate(result);
		
		log.info("Saving vehicle details...");
		Vehicle vehicle = vehicleService.create(_vehicle);
		
		log.info("Vehicle details saved successfully");
		return ok(vehicle);
	}
	
	/**
	 * Get the vehicle with given id
	 * @statuscode 404 Vehicle not found for given id
	 * @param id The vehicle id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException 
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity find(@PathVariable("id") String id) throws Exception {
		
		log.info("Getting vehicle details for {} " + id);
		Vehicle vehicle = vehicleService.find(id);
		
		if(vehicle != null){
			log.info("Vehicle details found for {} " + id);
			return ok(vehicle);	
		}
		
		throw new ObjectNotFoundException("vehicle.not.found");
	}
	
	/**
	 * Update existing vehicle
	 * @statuscode 404 Vehicle not found for given id
	 * @param id The id of the vehicle that will be updated
	 * @param _vehicle The vehicle details that will be updated
	 * @param result BindingResult object that holds validation results.
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException
	 */
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity update(@PathVariable("id") String id, @RequestBody @Valid Vehicle _vehicle, BindingResult result) throws Exception{
		Vehicle vehicle = vehicleService.find(id);
		
		if(vehicle != null){
			validate(result);
			
			log.info("Updating vehicle details for {} " + id);
			vehicle = vehicleService.update(vehicle, _vehicle);
			
			log.info("Vehicle details updated successfully");
			return ok(vehicle, "vehicle.update.success");
		}
		
		throw new ObjectNotFoundException("vehicle.not.found");
	}
	
	/**
	 * Delete vehicle with given id
	 * @statuscode 404 Vehicle not found for given id
	 * @param id The vehicle id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException
	 */
	@RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity delete(@PathVariable("id") String id) throws Exception{
		log.info("Deleting vehicle for {} " + id);
		Vehicle vehicle = vehicleService.find(id);
		
		if(vehicle != null){
			vehicleService.delete(vehicle);
		
			log.info("Vehicle deleted for {} " + id);
			return ok(vehicle, "vehicle.delete.success");
		}
		
		throw new ObjectNotFoundException("vehicle.not.found");
	}
	
	/**
	 * Get list of vehicles
	 * @param start First index of results. Default value is 1.
	 * @param end Last index of results. Default value is 10.
	 * @param sortBy Name of the field that perform the sorting. Default value is id field.
	 * @param sortType Type of sorting. The valid values are either asc or desc. Default value is asc.
	 * @return Object of ResponseEntity
	 * @throws Exception throws Exception
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity list(@RequestParam(value = "start", defaultValue = "1") int start,
											@RequestParam(value = "end", defaultValue = "10") int end,
											@RequestParam(value = "sortBy", defaultValue = "id") String sortBy,
											@RequestParam(value = "sortType", defaultValue = "asc") String sortType) throws Exception{
		log.info("Getting vehicles list...");
		Page<Vehicle> vehicles = vehicleService.list(start, end, sortBy, sortType);
		
		log.info("Vehicles found {} " + vehicles.getContent().size());
		return ok(vehicles);
	}
}
