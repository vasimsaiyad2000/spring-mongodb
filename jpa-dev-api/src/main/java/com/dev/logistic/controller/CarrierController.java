package com.dev.logistic.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dev.logistic.entity.AddressEnvelop;
import com.dev.logistic.entity.Carrier;
import com.dev.logistic.exception.BadRequestException;
import com.dev.logistic.exception.ObjectNotFoundException;
import com.dev.logistic.service.AddressEnvelopService;
import com.dev.logistic.service.CarrierService;
import com.dev.logistic.utils.ResponseEntity;
/**
 * This is a rest api class that provide carrier services to client.
 * Any client can access carrier api through end point url.
 * 
 * <p>
 * Class perform below operations.
 * 
 * <p> 
 * 1. Save carrier
 * <p>
 * 2. Find carrier 
 * <p>
 * 3. Update carrier
 * <p>
 * 4. Delete carrier
 * <p>
 * 5. List carriers
 * 
 * @author vasim
 *
 */
@Controller
@RequestMapping(value = "/api/carrier")
public class CarrierController extends BaseController<Carrier> {

	private static Logger log = Logger.getLogger(CarrierController.class);
	
	@Autowired
	private CarrierService carrierService;
	
	@Autowired
	private AddressEnvelopService addressEnvelopService;
	
	/**
	 * Create new carrier
	 * @param _carrier The carrier that will be created
	 * @param result BindingResult that hold validation results
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException 
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity create(@RequestBody @Valid Carrier _carrier, BindingResult result) throws Exception{
		
		validate(result);
		
		// Saving carrier address
		log.info("Saving carrier address...");
		AddressEnvelop addressEnvelop = addressEnvelopService.create(_carrier.getAddressEnvelop());
		log.info("Carrier address saved successfully");
		
		// Saving carrier
		log.info("Saving carrier details....");
		_carrier.setAddressEnvelop(addressEnvelop);
		Carrier carrier = carrierService.create(_carrier);
		
		log.info("Carrier saved successfully");
		return ok(carrier, "carrier.save.success");
	}
		
	/**
	 * Get the carrier with given id
	 * @statuscode 404 Carrier not found for given id
	 * @param id The carrier id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException 
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity find(@PathVariable("id") String id) throws Exception {
		log.info("Getting carrier for {} " + id);
		Carrier carrier = carrierService.find(id);
		
		if(carrier != null){
			log.info("Carrier found for {} " + id);
			return ok(carrier);	
		}
		
		throw new ObjectNotFoundException("carrier.not.found");
	}
	
	/**
	 * Update existing carrier
	 * @statuscode 404 Carrier not found for given id
	 * @param id The id of the carrier that will be updated
	 * @param _carrier The carrier details that will be updated
	 * @param result BindingResult object that holds validation results.
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException
	 */
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity update(@PathVariable("id") String id, @RequestBody @Valid Carrier _carrier, BindingResult result) throws Exception{
		
		Carrier carrier = carrierService.find(id);
		
		if(carrier != null){
			
			validate(result);
			
			// Updating carrier address
			log.info("Updating carrier address for {} " + carrier.getAddressEnvelop().getId());
			AddressEnvelop address = addressEnvelopService.update(carrier.getAddressEnvelop(), _carrier.getAddressEnvelop());
			log.info("Carrier address updated for {} " + carrier.getAddressEnvelop().getId());
			
			// Updating carrier
			log.info("Updating carrier for {} " +id);
			carrier.setAddressEnvelop(address);
			carrier = carrierService.update(carrier, _carrier);
			
			log.info("Carrier updated for {} " + id);
			return ok(carrier, "carrier.update.success");
		}
		
		throw new ObjectNotFoundException("carrier.not.found");
	}
	
	/**
	 * Delete carrier with given id
	 * @statuscode 404 Carrier not found for given id
	 * @param id The carrier id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException
	 */
	@RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity delete(@PathVariable("id") String id) throws Exception{
		log.info("Deleting carrier for {} " +id);
		Carrier carrier = carrierService.find(id);

		if(carrier != null){
			// Deleting carrier address
			addressEnvelopService.delete(carrier.getAddressEnvelop());
			
			// Deleting carrier
			carrierService.delete(carrier);
			log.info("Carrier deleted for {} " +id);
			return ok(carrier, "carrier.delete.success");
		}
		
		throw new ObjectNotFoundException("carrier.not.found");
	}
	
	/**
	 * Get list of carriers
	 * @param start First index of results. Default value is 1.
	 * @param end Last index of results. Default value is 10.
	 * @param sortBy Name of the field that perform the sorting. Default value is id field.
	 * @param sortType Type of sorting. The valid values are either asc or desc. Default value is asc.
	 * @return Object of ResponseEntity
	 * @throws Exception throws Exception
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity list(@RequestParam(value = "start", defaultValue = "1") int start,
											 @RequestParam(value = "end", defaultValue = "10") int end,
											 @RequestParam(value = "sortBy", defaultValue = "id") String sortBy,
											 @RequestParam(value = "sortType", defaultValue = "asc") String sortType) throws Exception{
		log.info("Getting carriers list...");
		Page<Carrier> carriers = carrierService.list(start, end, sortBy, sortType);
		
		log.info("Carriers found {} " + carriers.getContent().size());
		return ok(carriers);
	}
}
