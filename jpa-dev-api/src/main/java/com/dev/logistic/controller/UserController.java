package com.dev.logistic.controller;


import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dev.logistic.entity.User;
import com.dev.logistic.exception.BadRequestException;
import com.dev.logistic.exception.ObjectNotFoundException;
import com.dev.logistic.service.UserService;
import com.dev.logistic.utils.ResponseEntity;

/**
 * This is a rest api class that provide user services to client.
 * Any client can access user api through end point url.
 * 
 * <p>
 * Class perform below operations.
 * 
 * <p> 
 * 1. Save user
 * <p>
 * 2. Find user 
 * <p>
 * 3. Update user
 * <p>
 * 4. Delete user 
 * <p>
 * 5. List users
 * 
 * @author vasim
 *
 */

@Controller
@RequestMapping(value = "/api/user")
public class UserController extends BaseController<User>{
	
	private static Logger log = Logger.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	/**
	 * Create new user
	 * @param _user The user that will be created
	 * @param result BindingResult that hold validation results
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException 
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity create(@RequestBody @Valid User _user, BindingResult result) throws Exception{
		
		validate(_user, result);
		
		log.info("Saving user details...");
		User user = userService.create(_user);
		
		log.info("User details saved successfully");
		return ok(user, "user.save.success");
	}
		
	/**
	 * Get the user with given id
	 * @statuscode 404 User not found for given id
	 * @param id The user id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException 
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity find(@PathVariable("id") String id) throws Exception {
		log.info("Getting user details for {} " + id);
		User user = userService.find(id);
		
		if(user != null){
			log.info("User details found for {} " + id);
			return ok(user);	
		}
		
		throw new ObjectNotFoundException("user.not.found");
	}
	
	/**
	 * Update existing user
	 * @statuscode 404 User not found for given id
	 * @param id The id of the user that will be updated
	 * @param _user The user details that will be updated
	 * @param result BindingResult object that holds validation results.
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException
	 */
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity update(@PathVariable("id") String id, @RequestBody  @Valid User _user, BindingResult result) throws Exception{
		
		User user = userService.find(id);
		
		if(user != null){
			validate(_user, result);
			
			log.info("Updating user details for {} " + id);
			user = userService.update(user, _user);
			
			log.info("User details updated successfully");
			return ok(user, "user.update.success");
		}
		
		throw new ObjectNotFoundException("user.not.found");
	}
	
	/**
	 * Delete user with given id
	 * @statuscode 404 User not found for given id
	 * @param id The user id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException
	 */
	@RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity delete(@PathVariable("id") String id) throws Exception{
		log.info("Deleting user for {} " + id);
		User user = userService.find(id);
		
		if(user != null){
			userService.delete(user);
			
			log.info("User deleted for {} " +id);
			return ok(user, "user.delete.success");
		}
		
		throw new ObjectNotFoundException("user.not.found");
	}
	
	/**
	 * Get list of users
	 * @param start First index of results. Default value is 1.
	 * @param end Last index of results. Default value is 10.
	 * @param sortBy Name of the field that perform the sorting. Default value is id field.
	 * @param sortType Type of sorting. The valid values are either asc or desc. Default value is asc.
	 * @return Object of ResponseEntity
	 * @throws Exception throws Exception
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity list(@RequestParam(value = "start", defaultValue = "1") int start,
											 @RequestParam(value = "end", defaultValue = "10") int end,
											 @RequestParam(value = "sortBy", defaultValue = "id") String sortBy,
											 @RequestParam(value = "sortType", defaultValue = "asc") String sortType) throws Exception{
		
		log.info("Getting users list...");
		Page<User> users = userService.list(start, end, sortBy, sortType);
		
		log.info("Users found {} " + users.getContent().size());
		return ok(users);
	}

	private void validate(User user, BindingResult result) throws BadRequestException{
		
		if(userService.isEmailExist(user)){
			log.info("User with email " + user.getEmail() + " already exist");
			result.rejectValue("email", "Unique", "user.email.exist");
		}
		
		if(userService.isUserNameExist(user)){
			log.info("User with username " + user.getUserName() + " already exist");
			result.rejectValue("userName", "Unique", "user.username.exist");
		}
		
		validate(result);
	}
}
