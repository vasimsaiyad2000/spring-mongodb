package com.dev.logistic.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dev.logistic.entity.AddressEnvelop;
import com.dev.logistic.entity.Driver;
import com.dev.logistic.entity.Invoicee;
import com.dev.logistic.exception.BadRequestException;
import com.dev.logistic.exception.ObjectNotFoundException;
import com.dev.logistic.service.AddressEnvelopService;
import com.dev.logistic.service.DriverService;
import com.dev.logistic.service.InvoiceeService;
import com.dev.logistic.utils.ResponseEntity;
/**
 * This is a rest api class that provide driver services to client.
 * Any client can access driver api through end point url.
 * 
 * <p>
 * Class perform below operations.
 * 
 * <p> 
 * 1. Save driver
 * <p>
 * 2. Find driver 
 * <p>
 * 3. Update driver
 * <p>
 * 4. Delete driver
 * <p>
 * 5. List drivers
 * 
 * @author vasim
 *
 */
@Controller
@RequestMapping(value = "/api/driver")
public class DriverController extends BaseController<Driver> {

	private static Logger log = Logger.getLogger(DriverController.class);
	
	@Autowired
	private DriverService driverService;
	
	@Autowired
	private AddressEnvelopService addressEnvelopService;
	
	/**
	 * Create new driver
	 * @param _driver The driver that will be created
	 * @param result BindingResult that hold validation results
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException 
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity create(@RequestBody @Valid Driver _driver, BindingResult result) throws Exception{
	
		validate(result);

		// Saving driver address
		log.info("Saving driver address....");
		AddressEnvelop addressEnvelop = addressEnvelopService.create(_driver.getAddressEnvelop());
		log.info("Driver address saved successfully");
		
		// Saving driver
		log.info("Saving driver details...");
		_driver.setAddressEnvelop(addressEnvelop);
		Driver driver = driverService.create(_driver);
		
		log.info("Driver saved successfully");
		return ok(driver, "driver.save.success");
	}
	
	/**
	 * Get the driver with given id
	 * @statuscode 404 Driver not found for given id
	 * @param id The driver id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException 
	 */	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity find(@PathVariable("id") String id) throws Exception {
		log.info("Getting driver for {} " +id);
		Driver driver = driverService.find(id);
		
		if(driver != null){
			log.info("Driver found for {} " + id);
			return ok(driver);	
		}
		
		throw new ObjectNotFoundException("driver.not.found");
	}
	
	/**
	 * Update existing driver
	 * @statuscode 404 Driver not found for given id
	 * @param id The id of the driver that will be updated
	 * @param _driver The driver details that will be updated
	 * @param result BindingResult object that holds validation results.
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException
	 */
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity update(@PathVariable("id") String id, @RequestBody @Valid Driver _driver, BindingResult result) throws Exception{

		Driver driver = driverService.find(id);
		
		if(driver != null){
			
			validate(result);

			log.info("Updating driver for {} " + id);

			// Updating driver address
			log.info("Updating driver address for {} " + driver.getAddressEnvelop().getId());
			AddressEnvelop address = addressEnvelopService.update(driver.getAddressEnvelop(), _driver.getAddressEnvelop());
			log.info("Driver address updated for {} " + driver.getAddressEnvelop().getId());
			
			driver.setAddressEnvelop(address);
			log.info("Driver updated for {}" + id);
			return ok(driver, "driver.update.success");
		}
		
		throw new ObjectNotFoundException("driver.not.found");
	
	}
	
	/**
	 * Delete driver with given id
	 * @statuscode 404 Driver not found for given id
	 * @param id The driver id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException
	 */
	@RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity delete(@PathVariable("id") String id) throws Exception{
		log.info("Deleting driver for {} " + id);
		Driver driver = driverService.find(id);
		
		if(driver != null){
			// Deleting driver address
			log.info("Deleting driver address...");
			addressEnvelopService.delete(driver.getAddressEnvelop());
			log.info("Driver address deleted");
			
			// Deleting driver
			driverService.delete(driver);
			log.info("Driver deleted for {} " +id);
			return ok(driver, "driver.delete.success");
		}
		
		throw new ObjectNotFoundException("driver.not.found");
	}
	
	/**
	 * Get list of drivers
	 * @param start First index of results. Default value is 1.
	 * @param end Last index of results. Default value is 10.
	 * @param sortBy Name of the field that perform the sorting. Default value is id field.
	 * @param sortType Type of sorting. The valid values are either asc or desc. Default value is asc.
	 * @return Object of ResponseEntity
	 * @throws Exception throws Exception
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity list(@RequestParam(value = "start", defaultValue = "1") int start,
											 @RequestParam(value = "end", defaultValue = "10") int end,
											 @RequestParam(value = "sortBy", defaultValue = "id") String sortBy,
											 @RequestParam(value = "sortType", defaultValue = "asc") String sortType) throws Exception{
		log.info("Getting driver list...");
		Page<Driver> drivers = driverService.list(start, end, sortBy, sortType);
		
		log.info("Drivers found {} " + drivers.getContent().size());
		return ok(drivers);
	}

}
