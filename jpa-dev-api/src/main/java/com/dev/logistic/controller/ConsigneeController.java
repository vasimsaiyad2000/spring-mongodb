package com.dev.logistic.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dev.logistic.entity.AddressEnvelop;
import com.dev.logistic.entity.Consignee;
import com.dev.logistic.exception.BadRequestException;
import com.dev.logistic.exception.ObjectNotFoundException;
import com.dev.logistic.service.AddressEnvelopService;
import com.dev.logistic.service.ConsigneeService;
import com.dev.logistic.utils.ResponseEntity;
/**
 * This is a rest api class that provide consignee services to client.
 * Any client can access consignee api through end point url.
 * 
 * <p>
 * Class perform below operations.
 * 
 * <p> 
 * 1. Save consignee
 * <p>
 * 2. Find consignee 
 * <p>
 * 3. Update consignee
 * <p>
 * 4. Delete consignee
 * <p>
 * 5. List consignees
 * 
 * @author vasim
 *
 */
@Controller
@RequestMapping(value = "/api/consignee")
public class ConsigneeController extends BaseController<Consignee>{
	
	private static Logger log = Logger.getLogger(ConsigneeController.class);
	
	@Autowired
	private ConsigneeService consigneeService;
	
	@Autowired
	private AddressEnvelopService addressEnvelopService;
	
	
	/**
	 * Create new consignee
	 * @param _consignee The consignee that will be created
	 * @param result BindingResult that hold validation results
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException 
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity create(@RequestBody @Valid Consignee _consignee, BindingResult result) throws Exception{
		
		validate(result);
		
		// Saving consignee address
		log.info("Saving consignee address....");
		AddressEnvelop addressEnvelop = addressEnvelopService.create(_consignee.getAddressEnvelop());
		log.info("Consignee address saved successfully");
		
		// Saving consignee
		log.info("Saving consignee details....");
		_consignee.setAddressEnvelop(addressEnvelop);
		Consignee consignee = consigneeService.create(_consignee);
		
		log.info("Consignee saved successfully");
		return ok(consignee, "consignee.save.success");
	}
	
	/**
	 * Get the consignee with given id
	 * @statuscode 404 Consignee not found for given id
	 * @param id The consignee id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException 
	 */	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity find(@PathVariable("id") String id) throws Exception {
		log.info("Getting consignee for {} " + id);
		Consignee consignee = consigneeService.find(id);
		
		if(consignee != null){
			log.info("Consignee found for {} " +id);
			return ok(consignee);	
		}
		
		throw new ObjectNotFoundException("consignee.not.found");
	}
	
	/**
	 * Update existing consignee
	 * @statuscode 404 Consignee not found for given id
	 * @param id The id of the consignee that will be updated
	 * @param _consignee The consignee details that will be updated
	 * @param result BindingResult object that holds validation results.
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException
	 */
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity update(@PathVariable("id") String id, @RequestBody @Valid Consignee _consignee, BindingResult result) throws Exception{
		Consignee consignee = consigneeService.find(id);
		
		if(consignee != null){
			
			validate(result);
			
			log.info("Updating consignee for {} " +id);
			
			// Updating consignee address
			AddressEnvelop address = addressEnvelopService.update(consignee.getAddressEnvelop(), _consignee.getAddressEnvelop());
			consignee.setAddressEnvelop(address);
			
			log.info("Consignee updated for {} " + id);
			return ok(consignee, "consignee.update.success");
		}
		
		throw new ObjectNotFoundException("consignee.not.found");
	}
	
	/**
	 * Delete consignee with given id
	 * @statuscode 404 Consignee not found for given id
	 * @param id The consignee id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException
	 */
	@RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity delete(@PathVariable("id") String id) throws Exception{
		log.info("Deleting consignee for {} " +id);
		Consignee consignee = consigneeService.find(id);

		if(consignee != null){
			// Deleting consignee address
			log.info("Deleting consignee address for {}" + consignee.getAddressEnvelop().getId());
			addressEnvelopService.delete(consignee.getAddressEnvelop());
			log.info("consignee address deleted");
			
			// Deleting consignee
			consigneeService.delete(consignee);
			log.info("Consignee delete for {} " +id);
			return ok(consignee, "consignee.delete.success");
		}
		
		throw new ObjectNotFoundException("consignee.not.found");
	}
	
	/**
	 * Get list of consignees
	 * @param start First index of results. Default value is 1.
	 * @param end Last index of results. Default value is 10.
	 * @param sortBy Name of the field that perform the sorting. Default value is id field.
	 * @param sortType Type of sorting. The valid values are either asc or desc. Default value is asc.
	 * @return Object of ResponseEntity
	 * @throws Exception throws Exception
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity list(@RequestParam(value = "start", defaultValue = "1") int start,
											 @RequestParam(value = "end", defaultValue = "10") int end,
											 @RequestParam(value = "sortBy", defaultValue = "id") String sortBy,
											 @RequestParam(value = "sortType", defaultValue = "asc") String sortType) throws Exception{
		
		log.info("Getting consignee list...");
		Page<Consignee> consignees = consigneeService.list(start, end, sortBy, sortType);
		
		log.info("Consignee found {} " + consignees.getContent().size());
		return ok(consignees);
	}

}
