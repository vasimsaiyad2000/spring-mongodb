package com.dev.logistic.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dev.logistic.command.LoadInfoCommand;
import com.dev.logistic.constants.LoadStatus;
import com.dev.logistic.entity.LoadInfo;
import com.dev.logistic.exception.BadRequestException;
import com.dev.logistic.exception.ObjectNotFoundException;
import com.dev.logistic.service.LoadInfoService;
import com.dev.logistic.utils.ResponseEntity;
import com.dev.logistic.validation.LoadInfoValidator;


/**
 * This is a rest api class that provide loadinfo services to client.
 * Any client can access loadinfo api through end point url.
 * 
 * <p>
 * Class perform below operations.
 * 
 * <p> 
 * 1. Save loadinfo
 * <p>
 * 2. Find loadinfo
 * <p>
 * 3. Update loadinfo
 * <p>
 * 4. Delete loadinfo
 * <p>
 * 5. List loadinfo
 * <p>
 * 6. Search loadinfo based on given id, status or type
 * 
 * 
 * @author vasim
 *
 */
@Controller
@RequestMapping(value = "/api/loadinfo")
public class LoadInfoController extends BaseController<LoadInfoCommand> {

	private static Logger log = Logger.getLogger(LoadInfoController.class);
	
	@Autowired
	private LoadInfoService loadInfoService;
	
	@Autowired
	private LoadInfoValidator validator;
	
	/**
	 * Create new loadinfo
	 * @param command The LoadInfo that will be created
	 * @param result BindingResult that hold validation results
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException 
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity create(@RequestBody @Valid LoadInfoCommand command, BindingResult result) throws Exception{

		validate(command, result);
		
		log.info("Saving loadinfo details...");
		
		LoadInfo loadInfo = copyLoadInfoFields(command);
		loadInfo = loadInfoService.create(loadInfo);
		
		log.info("Loadinfo saved successfully");
		return ok(loadInfo, "loadinfo.save.success");
	}
	
	/**
	 * Get the loadinfo with given id
	 * @statuscode 404 LoadInfo not found for given id
	 * @param id The loadinfo id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException 
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity find(@PathVariable("id") String id) throws Exception {
		log.info("Getting loadinfo details for {} "  + id);
		LoadInfo loadInfo = loadInfoService.find(id);
		
		if(loadInfo != null){
			log.info("Loadinfo details found for {}" + id);
			return ok(loadInfo);	
		}
		
		throw new ObjectNotFoundException("loadinfo.not.found");
	}
	
	/**
	 * Update existing loadinfo
	 * @statuscode 404 loadinfo not found for given id
	 * @param id The id of the loadinfo that will be updated
	 * @param command The loadinfo details that will be updated
	 * @param result BindingResult object that holds validation results.
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException
	 */
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity update(@PathVariable("id") String id, @RequestBody @Valid LoadInfoCommand command, BindingResult result) throws Exception{

		LoadInfo loadInfo = loadInfoService.find(id);
		
		if(loadInfo != null){
			validate(command, result);
			
			log.info("Updating loadinfo details for {}" + id);

			LoadInfo _loadInfo = copyLoadInfoFields(command);
			loadInfo = loadInfoService.update(loadInfo, _loadInfo);
			
			log.info("Loadinfo updated for {}" +id);
			return ok(loadInfo, "loadinfo.update.success");
		}
		
		throw new ObjectNotFoundException("loadinfo.not.found");
	}
	
	/**
	 * Delete loadinfo with given id
	 * @statuscode 404 LoadInfo not found for given id
	 * @param id The loadinfo id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException
	 */
	@RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity delete(@PathVariable("id") String id) throws Exception{
		log.info("Deleting loadinfo details for {}" + id);
		LoadInfo loadInfo = loadInfoService.find(id);

		if(loadInfo != null){			
			loadInfoService.delete(loadInfo);
			
			log.info("Loadinfo deleted for {}" + id);
			return ok(loadInfo, "loadinfo.delete.success");
		}
		
		throw new ObjectNotFoundException("loadinfo.not.found");
	}
	
	/**
	 * Get list of loadinfo
	 * @param start First index of results. Default value is 1.
	 * @param end Last index of results. Default value is 10.
	 * @param sortBy Name of the field that perform the sorting. Default value is id field.
	 * @param sortType Type of sorting. The valid values are either asc or desc. Default value is asc.
	 * @return Object of ResponseEntity
	 * @throws Exception throws Exception
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity list(@RequestParam(value = "start", defaultValue = "1") int start,
											 @RequestParam(value = "end", defaultValue = "10") int end,
											 @RequestParam(value = "sortBy", defaultValue = "id") String sortBy,
											 @RequestParam(value = "sortType", defaultValue = "asc") String sortType) throws Exception{
		log.info("Getting loadinfo details...");
		Page<LoadInfo> loadInfoList = loadInfoService.list(start, end, sortBy, sortType);
		
		log.info("Loadinfo found {}" + loadInfoList.getContent().size());
		return ok(loadInfoList);
	}

	/**
	 * Search loadinfo with given loadinfo id, type and status.
	 * @param by Loadinfo field that perform search operation.
	 * @param value Loadinfo field value to search from given field
	 * @return Object of ResponseEntity
	 */
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity search(@RequestParam("by") String by, @RequestParam("value") String value){
		List<LoadInfo> results = new ArrayList<LoadInfo>();
		
		log.info("Searching loadinfo  by " + by + " for value {} " + value);
		
		if(StringUtils.isNotBlank(by)){
			switch (by.toLowerCase()) {
				case "id" :
					results = loadInfoService.searchById(value);
					break;
				case "status" :
					results = loadInfoService.searchByStatus(LoadStatus.valueOfStatus(value));
					break;
				case "type" :
					results = loadInfoService.searchByType(Integer.parseInt(value));
					break;
				default:
					break;
			}
		}
	
		log.info("search results found {}" + results.size());
		return ok(results);
	}
	
	private LoadInfo copyLoadInfoFields(LoadInfoCommand command){
		LoadInfo loadInfo = new LoadInfo();
		
		if(StringUtils.isNotBlank(command.getId())) 
			loadInfo.setId(command.getId());
		if(StringUtils.isNotBlank(command.getWorkOrder()))
			loadInfo.setWorkOrder(Integer.parseInt(command.getWorkOrder()));
		if(StringUtils.isNotBlank(command.getType()))
			loadInfo.setType(Integer.parseInt(command.getType()));
		if(StringUtils.isNotBlank(command.getRate()))
			loadInfo.setRate(Float.parseFloat(command.getRate()));
		if(StringUtils.isNotBlank(command.getNoOfUnits()))
			loadInfo.setNoOfUnits(Float.parseFloat(command.getNoOfUnits()));
		if(StringUtils.isNotBlank(command.getP_by_d()))
			loadInfo.setP_by_d(Float.parseFloat(command.getP_by_d()));
		if(StringUtils.isNotBlank(command.getF_s_c()))
			loadInfo.setF_s_c(Float.parseFloat(command.getF_s_c()));
		if(StringUtils.isNotBlank(command.getOtherCharges()))
			loadInfo.setOtherCharges(Float.parseFloat(command.getOtherCharges()));
		if(StringUtils.isNotBlank(command.getTotalRate()))
			loadInfo.setTotalRate(Float.parseFloat(command.getTotalRate()));
		if(StringUtils.isNotBlank(command.getDirverFlatRate()))
			loadInfo.setDirverFlatRate(Float.parseFloat(command.getDirverFlatRate()));
		if(StringUtils.isNotBlank(command.getProratedMiles()))
			loadInfo.setProratedMiles(Float.parseFloat(command.getProratedMiles()));
		if(StringUtils.isNotBlank(command.getProratedEmptyMiles()))
			loadInfo.setProratedEmptyMiles(Float.parseFloat(command.getProratedEmptyMiles()));
		if(StringUtils.isNotBlank(command.getDirverMiles()))
			loadInfo.setDirverMiles(Float.parseFloat(command.getDirverMiles()));
		if(StringUtils.isNotBlank(command.getDirverEmptyMiles()))
			loadInfo.setDirverEmptyMiles(Float.parseFloat(command.getDirverEmptyMiles()));
		if(StringUtils.isNotBlank(command.getDriverHourly()))
			loadInfo.setDriverHourly(Float.parseFloat(command.getDriverHourly()));
		
		loadInfo.setUsername(command.getUsername());
		loadInfo.setSalesRepid(command.getSalesRepid());
		loadInfo.setEquipmentId(command.getEquipmentId());
		loadInfo.setDispatcherId(command.getDispatcherId());
		loadInfo.setStatus(LoadStatus.valueOfStatus(command.getStatus()));
		loadInfo.setDriver(command.getDriver());
		loadInfo.setCarrier(command.getCarrier());
		loadInfo.setInvoicee(command.getInvoicee());
		loadInfo.setConsignees(command.getConsignees());
		loadInfo.setShippers(command.getShippers());
		loadInfo.setVehicles(command.getVehicles());
		loadInfo.setBillofLandings(command.getBillofLandings());
		loadInfo.setLoadinfoAddtionalNumbers(command.getLoadinfoAddtionalNumbers());
		
		return loadInfo;
	}
	
	private void validate(LoadInfoCommand command, BindingResult result) throws BadRequestException{
		validator.validate(command, result);
		validate(result);
	}
}
