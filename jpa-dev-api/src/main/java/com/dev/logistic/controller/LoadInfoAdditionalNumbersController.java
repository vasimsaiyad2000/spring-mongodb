package com.dev.logistic.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dev.logistic.entity.LoadInfoAdditionalNumbers;
import com.dev.logistic.exception.BadRequestException;
import com.dev.logistic.exception.ObjectNotFoundException;
import com.dev.logistic.service.LoadInfoAdditionalNumbersService;
import com.dev.logistic.utils.ResponseEntity;

/**
 * This is a rest api class that provide loadinfo addtional numbers services to client.
 * Any client can access loadinfo addtional numbers api through end point url.
 * 
 * <p>
 * Class perform below operations.
 * 
 * <p> 
 * 1. Save loadinfo addtional number
 * <p>
 * 2. Find loadinfo addtional number
 * <p>
 * 3. Update loadinfo addtional number
 * <p>
 * 4. Delete loadinfo addtional number
 * <p>
 * 5. List loadinfo addtional numbers
 * 
 * @author vasim
 *
 */

@Controller
@RequestMapping(value = "/api/loadinfoadditionalnumbers")
public class LoadInfoAdditionalNumbersController extends BaseController<LoadInfoAdditionalNumbers> {

	private static Logger log = Logger.getLogger(LoadInfoAdditionalNumbersController.class);
	
	@Autowired
	private LoadInfoAdditionalNumbersService lianService;
	
	/**
	 * Create new loadinfo addtional number
	 * @param _lian The loadinfoadditionalnumber that will be created
	 * @param result BindingResult that hold validation results
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException 
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity create(@RequestBody @Valid LoadInfoAdditionalNumbers _lian, BindingResult result) throws Exception {
		
		validate(result);
		
		log.info("Saving loadinfo additional numbers...");
		LoadInfoAdditionalNumbers lian = lianService.create(_lian);
		
		log.info("Loadinfo additional numbers saved successfully");
		return ok(lian, "lian.save.success");
	}

	/**
	 * Get the loadinfo additional number with given id
	 * @statuscode 404 LoadinfoAddtionalNumber not found for given id
	 * @param id The LoadinfoAddtionalNumber id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException 
	 */
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET) 
	public @ResponseBody ResponseEntity find(@PathVariable("id") String id) throws Exception {
		log.info("Getting Loadinfo additional numbers for {} " + id);
		LoadInfoAdditionalNumbers lian = lianService.find(id);
		
		if(lian != null){
			log.info("Loadinfo additional number found for {}" + id);
			return ok(lian);
		}
		
		throw new ObjectNotFoundException("lian.not.found");
	}

	/**
	 * Update existing LoadInfoAdditionalNumber
	 * @statuscode 404 LoadInfoAdditionalNumber not found for given id
	 * @param id The id of the LoadInfoAdditionalNumber that will be updated
	 * @param _lian The LoadInfoAdditionalNumber details that will be updated
	 * @param result BindingResult object that holds validation results.
	 * @return Object of ResponseEntity
	 * @throws Exception throws BadRequestException
	 */
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity update(@PathVariable("id") String id, @RequestBody @Valid LoadInfoAdditionalNumbers _lian, BindingResult result) throws Exception {
		
		LoadInfoAdditionalNumbers lian = lianService.find(id);
		
		if(lian != null){
			validate(result);
			
			log.info("Updating loadinfo additional number for {} " + id);
			lian = lianService.update(lian, _lian);
			
			log.info("Loadinfo additional numbers updated for {}" + id);
			return ok(lian, "lian.update.success");
		}
		
		throw new ObjectNotFoundException("lian.not.found");
	}

	/**
	 * Delete Loadinfoadditionalnumbers with given id
	 * @statuscode 404 Loadinfoadditionalnumbers not found for given id
	 * @param id The Loadinfoadditionalnumbers id
	 * @return Object of ResponseEntity
	 * @throws Exception throws ObjectNotFoundException
	 */
	@RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity delete(@PathVariable("id") String id) throws Exception {
		log.info("Deleting loadinfo additional numbers for {] " + id);
		LoadInfoAdditionalNumbers lian = lianService.find(id);
		
		if(lian != null){
			lianService.delete(lian);
			
			log.info("Loadinfo additional number deleted for {} " +id);
			return ok(lian, "lian.delete.success");
		}
		
		throw new ObjectNotFoundException("lian.not.found");
		
	}
	
	/**
	 * Get list of LoadInfoAdditionalNumbers
	 * @param start First index of results. Default value is 1.
	 * @param end Last index of results. Default value is 10.
	 * @param sortBy Name of the field that perform the sorting. Default value is id field.
	 * @param sortType Type of sorting. The valid values are either asc or desc. Default value is asc.
	 * @return Object of ResponseEntity
	 * @throws Exception throws Exception
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity list(@RequestParam(value = "start", defaultValue = "1") int start,
											 @RequestParam(value = "end", defaultValue = "10") int end,
											 @RequestParam(value = "sortBy", defaultValue = "id") String sortBy,
											 @RequestParam(value = "sortType", defaultValue = "asc") String sortType) throws Exception {
		
		log.info("Getting loadinfo additional numbers list...");
		Page<LoadInfoAdditionalNumbers> lians = lianService.list(start, end, sortBy, sortType);
		
		log.info("Loadinfo additional numbers found {}" + lians.getContent().size());
		return ok(lians);
	}
}
