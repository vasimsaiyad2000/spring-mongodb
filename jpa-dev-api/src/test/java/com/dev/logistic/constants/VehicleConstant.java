package com.dev.logistic.constants;

public class VehicleConstant {

	public static final String TYPE_REQUIRED_MESSAGE = "Type is required";
	public static final String TYPE_REQUIRED_KEY = "vehicle.type.required";
	public static final String VEHICLE_RESPONSE_ID = "$.response.id";
	public static final String VEHICLE_RESPONSE_TYPE = "$.response.type";
	public static final String VEHICLE_NOT_FOUND_KEY = "vehicle.not.found";
	public static final String VEHICLE_NOT_FOUND_MESSAGE = "Vehicle not found";
	public static final String VEHICLE_FIELD_ERRORS = "$.fieldErrors";
	public static final String VEHICLE_FIELD_ERRORS_FIELDS = "$.fieldErrors[*].field";
	public static final String VEHICLE_FIELD_ERRORS_MESSAGES = "$.fieldErrors[*].message";
	public static final String VEHICLE_TYPE_FIELD = "type";
	public static final String VEHICLE_TYPE_VALUE = "Heavy";
	
}
