package com.dev.logistic.controller;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import com.dev.logistic.utils.ResponseEntity;
import com.dev.logistic.utils.ResponseFactory;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/spring-servlet.xml", "file:src/main/webapp/WEB-INF/spring-security.xml"})
public abstract class AbstractControllerTest {

	protected MockMvc mockMvc;
	
	@Mock
	protected ResponseFactory responseFactory;

	@Mock
	protected MessageSource messageResource;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
}
