package com.dev.logistic.controller;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.UUID;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.dev.logistic.constants.VehicleConstant;
import com.dev.logistic.entity.Vehicle;
import com.dev.logistic.exception.AbstractExceptionHandler;
import com.dev.logistic.service.VehicleService;
import com.dev.logistic.utils.ResponseEntity;
import com.dev.logistic.utils.TestUtils;



public class VehicleControllerTest extends AbstractControllerTest{
	
	@Mock
	private VehicleService vehicleServiceMock;
	
	@InjectMocks
	private VehicleController controller;
	
	@Before
	public void setUp(){
		super.setUp();
		mockMvc = MockMvcBuilders.standaloneSetup(controller)
				.setControllerAdvice(new AbstractExceptionHandler(messageResource))
				.build();
	}
	
	@Test
	public void findById_Found_ShouldReturnFoundVehicle() throws Exception{
		Vehicle vehicle = mockVehicle();		
		
		when(vehicleServiceMock.find(vehicle.getId())).thenReturn(vehicle);
		when(responseFactory.ok(vehicle)).thenReturn(new ResponseEntity(vehicle));
		
		mockMvc.perform(get("/api/vehicle/{id}", vehicle.getId()))
			.andExpect(status().isOk())
			.andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath(VehicleConstant.VEHICLE_RESPONSE_ID, is(vehicle.getId())))
            .andExpect(jsonPath(VehicleConstant.VEHICLE_RESPONSE_TYPE, is(vehicle.getType())))
            .andDo(print());
		
		verify(vehicleServiceMock, times(1)).find(vehicle.getId());
		verifyNoMoreInteractions(vehicleServiceMock);
	}
	
	@Test
	public void findById_NotFound_ShouldReturnHttpStatus404() throws Exception{
		String id = UUID.randomUUID().toString();		
		
		when(vehicleServiceMock.find(id)).thenReturn(null);
		when(messageResource.getMessage(VehicleConstant.VEHICLE_NOT_FOUND_KEY, null, null)).thenReturn(VehicleConstant.VEHICLE_NOT_FOUND_MESSAGE);
		
		mockMvc.perform(get("/api/vehicle/{id}", id))
			.andExpect(status().isNotFound())
            .andDo(print());
		
		verify(vehicleServiceMock, times(1)).find(id);
		verifyNoMoreInteractions(vehicleServiceMock);
	}


	@Test
	public void deleteById_Found_ShouldDeleteAndReturnIt() throws Exception{

		Vehicle vehicle = mockVehicle();
		when(vehicleServiceMock.find(vehicle.getId())).thenReturn(vehicle);
		
		mockMvc.perform(delete("/api/vehicle/delete/{id}", vehicle.getId()))
			.andExpect(status().isOk())
            .andDo(print());
		
		verify(vehicleServiceMock, timeout(1)).delete(vehicle);
	}
	
	@Test
	public void deleteById_NotFound_ShouldReturnHttpStatus404() throws Exception{
		Vehicle vehicle = mockVehicle();
		
		when(vehicleServiceMock.find(vehicle.getId())).thenReturn(null);
		when(messageResource.getMessage(VehicleConstant.VEHICLE_NOT_FOUND_KEY, null, null)).thenReturn(VehicleConstant.VEHICLE_NOT_FOUND_MESSAGE);
		
		mockMvc.perform(delete("/api/vehicle/delete/{id}", vehicle.getId()))
			.andExpect(status().isNotFound())
			.andDo(print());
		
		verify(vehicleServiceMock, times(1)).find(vehicle.getId());
	}
	
	@Test
	public void add_NewVehicle_ShouldAddAndReturnAddedVehicle() throws Exception{
		
		Vehicle added = mockVehicle();
		Vehicle dtoVehicle = new Vehicle();
		dtoVehicle.setType(VehicleConstant.VEHICLE_TYPE_FIELD);
		
		when(vehicleServiceMock.create(any(Vehicle.class))).thenReturn(added);
		when(responseFactory.ok(added)).thenReturn(new ResponseEntity(added));
		
		mockMvc.perform(post("/api/vehicle/create")
					.contentType(TestUtils.APPLICATION_JSON_UTF8)
					.content(TestUtils.convertObjectToJsonBytes(dtoVehicle)))
				.andExpect(status().isCreated())
				.andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath(VehicleConstant.VEHICLE_RESPONSE_ID, is(added.getId())))
				.andExpect(jsonPath(VehicleConstant.VEHICLE_RESPONSE_TYPE, is(added.getType())))
				.andDo(print());

		ArgumentCaptor<Vehicle> captor = ArgumentCaptor.forClass(Vehicle.class);
		
		verify(vehicleServiceMock, timeout(1)).create(captor.capture());
		verifyNoMoreInteractions(vehicleServiceMock);
	}
	
	@Test
	public void add_NewVehicle_TypeFieldEmptyShouldReturnBadRequest() throws Exception{

		Vehicle dtoVehicle = new Vehicle();
		dtoVehicle.setType("");
		
		when(messageResource.getMessage(VehicleConstant.TYPE_REQUIRED_KEY, null, null)).thenReturn(VehicleConstant.TYPE_REQUIRED_MESSAGE);
		
		mockMvc.perform(post("/api/vehicle/create")
					.contentType(TestUtils.APPLICATION_JSON_UTF8)
					.content(TestUtils.convertObjectToJsonBytes(dtoVehicle)))
				.andExpect(status().isBadRequest())
				.andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath(VehicleConstant.VEHICLE_FIELD_ERRORS, hasSize(1)))
				.andExpect(jsonPath(VehicleConstant.VEHICLE_FIELD_ERRORS_FIELDS, containsInAnyOrder(VehicleConstant.VEHICLE_TYPE_FIELD)))
				.andExpect(jsonPath(VehicleConstant.VEHICLE_FIELD_ERRORS_MESSAGES, containsInAnyOrder(VehicleConstant.TYPE_REQUIRED_MESSAGE)))
				.andDo(print());

		verifyZeroInteractions(vehicleServiceMock);
	}
	
	private Vehicle mockVehicle(){
		Vehicle vehicle = new Vehicle();
		vehicle.setId(UUID.randomUUID().toString());
		vehicle.setType(VehicleConstant.VEHICLE_TYPE_FIELD);
		vehicle.setCreatedDate(new DateTime());
		vehicle.setLastModifiedDate(new DateTime());
		
		return vehicle;
	}
	
/*	private List<Vehicle> mockVehicles(){
		
		Vehicle vehicle01 = new Vehicle();
		vehicle01.setId(UUID.randomUUID().toString());
		vehicle01.setType("Medium");
		
		Vehicle vehicle02 = new Vehicle();
		vehicle02.setId(UUID.randomUUID().toString());
		vehicle02.setType("Small");
		
		return Arrays.asList(vehicle01, vehicle02);
	}*/
}
