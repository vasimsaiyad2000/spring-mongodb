package com.dev.logistic.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.dev.logistic.entity.User;

/**
 * This interface is responsible to perform mongodb operations for User.
 * @author vasim
 *
 */
public interface UserRepository extends MongoRepository<User, String> {

	public User findByUserName(String username);
	public User findByEmail(String email);
}
