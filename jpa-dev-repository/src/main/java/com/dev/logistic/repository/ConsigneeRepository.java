package com.dev.logistic.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.dev.logistic.entity.Consignee;

/**
 * This interface is responsible to perform mongodb operations for Consignee
 * @author vasim
 *
 */
public interface ConsigneeRepository extends MongoRepository<Consignee, String> {

}
