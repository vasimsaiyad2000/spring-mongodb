package com.dev.logistic.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.dev.logistic.entity.AddressEnvelop;

/**
 * This interface is responsible to perform mongodb operations for AddressEnvelop.
 * @author vasim
 *
 */
public interface AddressEnvelopRepository extends MongoRepository<AddressEnvelop, String> {

}
