package com.dev.logistic.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.dev.logistic.entity.BillofLanding;

/**
 * This interface is responsible to perform mongodb operations for BillofLanding.
 * @author vasim
 *
 */
public interface BillofLandingRepository extends MongoRepository<BillofLanding, String> {

}
