package com.dev.logistic.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.dev.logistic.constants.LoadStatus;
import com.dev.logistic.entity.LoadInfo;

/**
 * This interface is responsible to perform mongodb operations for LoadInfo
 * @author vasim
 *
 */
public interface LoadInfoRepository extends MongoRepository<LoadInfo, String> {

	/**
	 * This method find loadinfo by id
	 * @param id Loadinfo id
	 * @return List of LoadInfo 
	 */
	
	public List<LoadInfo> findById(String id);
	
	/**
	 * This method find loadinfo by status
	 * @param status Loadinfo status
	 * @return List of LoadInfo
	 */
	public List<LoadInfo> findByStatus(LoadStatus status);
	
	/**
	 * This method find loadinfo by type 
	 * @param type Loadinfo type
	 * @return List of LoadInfo
	 */
	public List<LoadInfo> findByType(int type);
}
