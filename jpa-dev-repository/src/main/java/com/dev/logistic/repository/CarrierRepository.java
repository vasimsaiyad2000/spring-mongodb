package com.dev.logistic.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.dev.logistic.entity.Carrier;

/**
 * This interface is responsible to perform mongodb operations for Carrier.
 * @author vasim
 *
 */
public interface CarrierRepository extends MongoRepository<Carrier, String> {

}
