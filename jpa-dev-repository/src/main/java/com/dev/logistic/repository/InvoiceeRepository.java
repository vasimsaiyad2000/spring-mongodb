package com.dev.logistic.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.dev.logistic.entity.Invoicee;

/**
 * This interface is responsible to perform mongodb operations for Invoicee.
 * @author vasim
 *
 */
public interface InvoiceeRepository extends MongoRepository<Invoicee, String> {

}
