package com.dev.logistic.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.dev.logistic.entity.LoadInfoAdditionalNumbers;

/**
 * This interface is responsible to perform mongodb operations for LoadInfoAdditionalNumbers.
 * @author vasim
 *
 */
public interface LoadInfoAdditionalNumbersRepository extends MongoRepository<LoadInfoAdditionalNumbers, String> {

}
