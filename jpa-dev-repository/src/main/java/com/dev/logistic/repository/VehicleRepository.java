package com.dev.logistic.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.dev.logistic.entity.Vehicle;

/**
 * This interface is responsible to perform the mongodb operations for vehicle
 * @author vasim
 *
 */
public interface VehicleRepository extends MongoRepository<Vehicle, String> {

}
