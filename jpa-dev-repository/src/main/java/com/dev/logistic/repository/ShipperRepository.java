package com.dev.logistic.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.dev.logistic.entity.Shipper;

/**
 * This interface is responsible to perform mongodb operations for Shipper.
 * @author vasim
 *
 */
public interface ShipperRepository extends MongoRepository<Shipper, String>{

}
