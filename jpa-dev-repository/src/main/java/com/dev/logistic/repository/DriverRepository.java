package com.dev.logistic.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.dev.logistic.entity.Driver;

/**
 * This interface is responsible to perform mongodb operations for Driver
 * @author vasim
 *
 */
public interface DriverRepository extends MongoRepository<Driver, String>{

}
